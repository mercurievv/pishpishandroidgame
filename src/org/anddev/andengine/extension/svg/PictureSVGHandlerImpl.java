package org.anddev.andengine.extension.svg;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Picture;
import android.graphics.RectF;
import org.anddev.andengine.extension.svg.adt.ISVGColorMapper;
import org.anddev.andengine.extension.svg.adt.SVGGradient;
import org.anddev.andengine.extension.svg.adt.SVGGroup;
import org.anddev.andengine.extension.svg.adt.SVGPaint;
import org.anddev.andengine.extension.svg.adt.SVGProperties;
import org.anddev.andengine.extension.svg.adt.filter.SVGFilter;
import org.anddev.andengine.extension.svg.adt.filter.element.ISVGFilterElement;
import org.anddev.andengine.extension.svg.util.SAXHelper;
import org.anddev.andengine.extension.svg.util.SVGCircleParser;
import org.anddev.andengine.extension.svg.util.SVGEllipseParser;
import org.anddev.andengine.extension.svg.util.SVGLineParser;
import org.anddev.andengine.extension.svg.util.SVGPolygonParser;
import org.anddev.andengine.extension.svg.util.SVGPolylineParser;
import org.anddev.andengine.extension.svg.util.SVGRectParser;
import org.anddev.andengine.extension.svg.util.SVGTransformParser;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.List;
import java.util.Stack;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.23.4
 * Time: 20:04
 */
public class PictureSVGHandlerImpl extends AbstarctSVGHandler {
    private Canvas currentCanvas;
    private Picture currentPicture;
    private final SVGPaint mSVGPaint;
    private final List<Picture> frames = new Vector<Picture>();
    private final Stack<Picture> pictures = new Stack<Picture>();
    private final Stack<Canvas> canvas = new Stack<Canvas>();
    private int svgWidth;
    private int svgHeight;

    public PictureSVGHandlerImpl(ISVGColorMapper pSVGColorMapper) {
        super();
        this.mSVGPaint = new SVGPaint(pSVGColorMapper);
    }

    public List<Picture> getPictures() {
        return frames;
    }

    @Override
    public void endElement(final String pNamespace, final String pLocalName, final String pQualifiedName) throws SAXException {
        if (pLocalName.equals(TAG_SVG)) {
            currentPicture.endRecording();
            if (frames.isEmpty())
                frames.add(currentPicture);
        }
        super.endElement(pNamespace, pLocalName, pQualifiedName);
    }

    protected void parseSVG(final Attributes pAttributes) {
        svgWidth = (int) Math.ceil(SAXHelper.getFloatAttribute(pAttributes, ATTRIBUTE_WIDTH, 0f));
        svgHeight = (int) Math.ceil(SAXHelper.getFloatAttribute(pAttributes, ATTRIBUTE_HEIGHT, 0f));
        frames.clear();
        currentPicture = new Picture();
        currentCanvas = currentPicture.beginRecording(svgWidth, svgHeight);
    }

    @Override
    protected void parseGroup(Attributes pAttributes) {
        if (SAXHelper.getStringAttribute(pAttributes, ATTRIBUTE_ID, "").startsWith("Frame-")) {
            currentPicture = new Picture();
            currentCanvas = currentPicture.beginRecording(svgWidth, svgHeight);
        }
        super.parseGroup(pAttributes);
    }

    @Override
    protected void parseGroupEnd() {
        if (!mSVGGroupStack.isEmpty()) {
            SVGGroup svgGroup = mSVGGroupStack.peek();
            if (svgGroup
                    .getSVGProperties()
                    .getStringAttribute(ATTRIBUTE_ID, "")
                    .startsWith("Frame-")) {
                currentPicture.endRecording();
                frames.add(currentPicture);
            }
        }
        super.parseGroupEnd();
    }

    protected void parsePath(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        this.mSVGPathParser.parse(svgProperties, this.currentCanvas, this.mSVGPaint);
        if (pushed) {
            this.popTransform();
        }
    }

    protected void parsePolygon(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        //final boolean pushed = this.pushTransform(pAttributes);
        SVGPolygonParser.parse(svgProperties, this.currentCanvas, this.mSVGPaint);
/*
        if (pushed) {
            this.popTransform();
        }
*/
    }

    protected void parsePolyline(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        SVGPolylineParser.parse(svgProperties, this.currentCanvas, this.mSVGPaint);
        if (pushed) {
            this.popTransform();
        }
    }

    protected void parseEllipse(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        SVGEllipseParser.parse(svgProperties, this.currentCanvas, this.mSVGPaint, this.mRect);
        if (pushed) {
            this.popTransform();
        }
    }

    protected void parseCircle(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        SVGCircleParser.parse(svgProperties, this.currentCanvas, this.mSVGPaint);
        if (pushed) {
            this.popTransform();
        }
    }

    protected void parseLine(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        SVGLineParser.parse(svgProperties, this.currentCanvas, this.mSVGPaint);
        if (pushed) {
            this.popTransform();
        }
    }

    protected void parseRect(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        SVGRectParser.parse(svgProperties, this.currentCanvas, this.mSVGPaint, this.mRect);
        if (pushed) {
            this.popTransform();
        }
    }

    protected boolean pushTransform(final Attributes pAttributes) {
        final String transform = SAXHelper.getStringAttribute(pAttributes, ATTRIBUTE_TRANSFORM);
        if (transform == null) {
            return false;
        } else {
            final Matrix matrix = SVGTransformParser.parseTransform(transform);
            this.currentCanvas.save();
            this.currentCanvas.concat(matrix);
            return true;
        }
    }

    protected void popTransform() {
        this.currentCanvas.restore();
    }

    public RectF getComputedBounds() {
        return this.mSVGPaint.getComputedBounds();
    }

    private SVGGradient mCurrentSVGGradient;
    private SVGFilter mCurrentSVGFilter;

    protected void parseFilter(final Attributes pAttributes) {
        this.mCurrentSVGFilter = this.mSVGPaint.parseFilter(pAttributes);
    }

    protected void parseFilterElementGaussianBlur(final Attributes pAttributes) {
        final ISVGFilterElement svgFilterElement = this.mSVGPaint.parseFilterElementGaussianBlur(pAttributes);
        this.mCurrentSVGFilter.addFilterElement(svgFilterElement);
    }

    protected void parseLinearGradient(final Attributes pAttributes) {
        this.mCurrentSVGGradient = this.mSVGPaint.parseGradient(pAttributes, SVGGradient.Type.LINEAR);
    }

    protected void parseRadialGradient(final Attributes pAttributes) {
        this.mCurrentSVGGradient = this.mSVGPaint.parseGradient(pAttributes, SVGGradient.Type.RADIAL);
    }

    protected void parseGradientStop(final Attributes pAttributes) {
        final SVGGradient.SVGGradientStop svgGradientStop = this.mSVGPaint.parseGradientStop(this.getSVGPropertiesFromAttributes(pAttributes));
        this.mCurrentSVGGradient.addSVGGradientStop(svgGradientStop);
    }

    @Override
    protected void parsePattern(Attributes attributes) {
        canvas.push(currentCanvas);

        currentCanvas = mSVGPaint.createCanvasForPatternFill(attributes);

    }

    @Override
    protected void parsePatternEnd() {
        currentCanvas = canvas.pop();
    }
}
