package org.anddev.andengine.extension.svg;

import android.graphics.Path;
import android.graphics.RectF;
import org.anddev.andengine.extension.svg.adt.SVGProperties;
import org.anddev.andengine.extension.svg.util.SAXHelper;
import org.anddev.andengine.extension.svg.util.SVGLineParser;
import org.anddev.andengine.extension.svg.util.SVGPolylineParser;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.23.4
 * Time: 20:18
 */
public class PathSVGHandlerImpl extends AbstarctSVGHandler {
    private final Map<String, List<Path>> groups = new HashMap<String, List<Path>>();
    private LinkedList<Path> currentGroup;
    private int groupLevel;
    private double width;
    private double height;


    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public List<Path> getPaths(String groupName) {
        return groups.get(groupName);
    }

    @Override
    protected void parseGroup(Attributes pAttributes) {
        if (groupLevel == 0) {
            currentGroup = new LinkedList<Path>();
            groups.put(SAXHelper.getStringAttribute(pAttributes, ATTRIBUTE_ID), currentGroup);
        }
        groupLevel++;
        super.parseGroup(pAttributes);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    protected void parseGroupEnd() {
        groupLevel--;
        super.parseGroupEnd();
    }

    @Override
    protected void parsePath(Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        Path path = this.mSVGPathParser.parse(svgProperties);
        currentGroup.add(path);
        if (pushed) {
            this.popTransform();
        }
    }

    @Override
    protected void parsePolygon(Attributes pAttributes) throws SAXException {
        throw new SAXException("This element parsing not impledmented yet: " + pAttributes.toString());
    }

    @Override
    protected void parsePolyline(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        Path path = SVGPolylineParser.parse(svgProperties);
        currentGroup.add(path);
        if (pushed) {
            this.popTransform();
        }
    }

    @Override
    protected void parseEllipse(Attributes pAttributes) throws SAXException {
        throw new SAXException("This element parsing not impledmented yet: " + pAttributes.toString());
    }

    @Override
    protected void parseCircle(Attributes pAttributes) throws SAXException {
        throw new SAXException("This element parsing not impledmented yet: " + pAttributes.toString());
    }

    @Override
    protected void parsePattern(Attributes attributes) throws SAXException {
        throw new SAXException("This element parsing not impledmented yet: " + attributes.toString());
    }

    @Override
    protected void parsePatternEnd() {
    }

    @Override
    protected void parseLine(final Attributes pAttributes) {
        final SVGProperties svgProperties = this.getSVGPropertiesFromAttributes(pAttributes);
        final boolean pushed = this.pushTransform(pAttributes);
        Path path = SVGLineParser.parse(svgProperties);
        currentGroup.add(path);
        if (pushed) {
            this.popTransform();
        }
    }

    @Override
    protected void parseRect(Attributes pAttributes) throws SAXException {
        throw new SAXException("This element parsing not impledmented yet: " + pAttributes.toString());
    }

    @Override
    protected boolean pushTransform(Attributes pAttributes) {
        return false;
    }

    @Override
    protected void popTransform() {
    }

    @Override
    protected void parseSVG(Attributes pAttributes) {
        width = Math.ceil(SAXHelper.getFloatAttribute(pAttributes, ATTRIBUTE_WIDTH, 0f));
        height = Math.ceil(SAXHelper.getFloatAttribute(pAttributes, ATTRIBUTE_HEIGHT, 0f));
        groups.clear();
        groupLevel = 0;
    }

    @Override
    public RectF getComputedBounds() {
        return null;
    }

    @Override
    protected void parseFilter(Attributes pAttributes) {

    }

    @Override
    protected void parseFilterElementGaussianBlur(Attributes pAttributes) {
    }

    @Override
    protected void parseLinearGradient(Attributes pAttributes) {
    }

    @Override
    protected void parseRadialGradient(Attributes pAttributes) {
    }

    @Override
    protected void parseGradientStop(Attributes pAttributes) {
    }
}
