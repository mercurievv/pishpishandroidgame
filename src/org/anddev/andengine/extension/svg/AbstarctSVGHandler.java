package org.anddev.andengine.extension.svg;

import android.graphics.RectF;
import org.anddev.andengine.extension.svg.adt.SVGGroup;
import org.anddev.andengine.extension.svg.adt.SVGProperties;
import org.anddev.andengine.extension.svg.util.SAXHelper;
import org.anddev.andengine.extension.svg.util.SVGPathParser;
import org.anddev.andengine.extension.svg.util.constants.ISVGConstants;
import org.anddev.andengine.util.Debug;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;


/**
 * @author Larva Labs, LLC
 *         (c) 2010 Nicolas Gramlich
 *         (c) 2011 Zynga Inc.
 * @author Nicolas Gramlich
 * @since 16:50:02 - 21.05.2011
 */
public abstract class AbstarctSVGHandler extends DefaultHandler implements ISVGConstants {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private boolean groupBoundsMode;
    private RectF mBounds;

    protected final Stack<SVGGroup> mSVGGroupStack = new Stack<SVGGroup>();
    protected final SVGPathParser mSVGPathParser = new SVGPathParser();

    private boolean mHidden;

    /**
     * Multi purpose dummy rectangle.
     */
    protected final RectF mRect = new RectF();

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public RectF getBounds() {
        return this.mBounds;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void startElement(final String pNamespace, String pLocalName, final String pQualifiedName, final Attributes pAttributes) throws SAXException {
        pLocalName = pQualifiedName;
        /* Ignore everything but rectangles in bounds mode. */
        if (this.groupBoundsMode) {
            this.parseBounds(pLocalName, pAttributes);
            return;
        }
        if (pLocalName.equals(TAG_SVG)) {
            this.parseSVG(pAttributes);
        } else if (pLocalName.equals(TAG_DEFS)) {
            // Ignore
        } else if (pLocalName.equals(TAG_GROUP)) {
            this.parseGroup(pAttributes);
        } else if (pLocalName.equals(TAG_PATTERN)) {
            this.parsePattern(pAttributes);
        } else if (pLocalName.equals(TAG_LINEARGRADIENT)) {
            this.parseLinearGradient(pAttributes);
        } else if (pLocalName.equals(TAG_RADIALGRADIENT)) {
            this.parseRadialGradient(pAttributes);
        } else if (pLocalName.equals(TAG_STOP)) {
            this.parseGradientStop(pAttributes);
        } else if (pLocalName.equals(TAG_FILTER)) {
            this.parseFilter(pAttributes);
        } else if (pLocalName.equals(TAG_FILTER_ELEMENT_FEGAUSSIANBLUR)) {
            this.parseFilterElementGaussianBlur(pAttributes);
        } else if (!this.mHidden) {
            if (pLocalName.equals(TAG_RECTANGLE)) {
                this.parseRect(pAttributes);
            } else if (pLocalName.equals(TAG_LINE)) {
                this.parseLine(pAttributes);
            } else if (pLocalName.equals(TAG_CIRCLE)) {
                this.parseCircle(pAttributes);
            } else if (pLocalName.equals(TAG_ELLIPSE)) {
                this.parseEllipse(pAttributes);
            } else if (pLocalName.equals(TAG_POLYLINE)) {
                this.parsePolyline(pAttributes);
            } else if (pLocalName.equals(TAG_POLYGON)) {
                this.parsePolygon(pAttributes);
            } else if (pLocalName.equals(TAG_PATH)) {
                this.parsePath(pAttributes);
            } else {
                Debug.d("Unexpected SVG tag: '" + pLocalName + "'.");
            }
        } else {
            Debug.d("Unexpected SVG tag: '" + pLocalName + "'.");
        }
    }

    @Override
    public void endElement(String pNamespace, String pLocalName, String pQualifiedName) throws SAXException {
        pLocalName = pQualifiedName;
        if (pLocalName.equals(TAG_GROUP)) {
            this.parseGroupEnd();
        }
        if (pLocalName.equals(TAG_PATTERN)) {
            this.parsePatternEnd();
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    protected abstract void parseSVG(Attributes pAttributes);

    private void parseBounds(final String pLocalName, final Attributes pAttributes) {
        if (pLocalName.equals(TAG_RECTANGLE)) {
            final float x = SAXHelper.getFloatAttribute(pAttributes, ATTRIBUTE_X, 0f);
            final float y = SAXHelper.getFloatAttribute(pAttributes, ATTRIBUTE_Y, 0f);
            final float width = SAXHelper.getFloatAttribute(pAttributes, ATTRIBUTE_WIDTH, 0f);
            final float height = SAXHelper.getFloatAttribute(pAttributes, ATTRIBUTE_HEIGHT, 0f);
            this.mBounds = new RectF(x, y, x + width, y + height);
        }
    }

    protected abstract void parseFilter(Attributes pAttributes);

    protected abstract void parseFilterElementGaussianBlur(Attributes pAttributes);

    protected abstract void parseLinearGradient(Attributes pAttributes);

    protected abstract void parseRadialGradient(Attributes pAttributes);

    protected abstract void parseGradientStop(Attributes pAttributes);

    protected abstract void parsePattern(Attributes attributes) throws SAXException;

    protected abstract void parsePatternEnd();

    protected void parseGroup(final Attributes pAttributes) {
        /* Check to see if this is the "bounds" layer. */
        if ("bounds".equals(SAXHelper.getStringAttribute(pAttributes, ATTRIBUTE_ID))) {
            this.groupBoundsMode = true;
        }

        final SVGGroup parentSVGGroup = (this.mSVGGroupStack.size() > 0) ? this.mSVGGroupStack.peek() : null;
        final boolean hasTransform = this.pushTransform(pAttributes);

        this.mSVGGroupStack.push(new SVGGroup(parentSVGGroup, this.getSVGPropertiesFromAttributes(pAttributes, true), hasTransform));

        this.updateHidden();
    }

    protected void parseGroupEnd() {
        if (this.groupBoundsMode) {
            this.groupBoundsMode = false;
        }

        /* Pop group transform if there was one pushed. */
        if (this.mSVGGroupStack.pop().hasTransform()) {
            this.popTransform();
        }
        this.updateHidden();
    }

    private void updateHidden() {
        if (this.mSVGGroupStack.size() == 0) {
            this.mHidden = false;
        } else {
            this.mSVGGroupStack.peek().isHidden();
        }
    }

    protected abstract void parsePath(Attributes pAttributes) throws SAXException;

    protected abstract void parsePolygon(Attributes pAttributes) throws SAXException;

    protected abstract void parsePolyline(Attributes pAttributes) throws SAXException;

    protected abstract void parseEllipse(Attributes pAttributes) throws SAXException;

    protected abstract void parseCircle(Attributes pAttributes) throws SAXException;

    protected abstract void parseLine(Attributes pAttributes) throws SAXException;

    protected abstract void parseRect(Attributes pAttributes) throws SAXException;

    protected SVGProperties getSVGPropertiesFromAttributes(final Attributes pAttributes) {
        return this.getSVGPropertiesFromAttributes(pAttributes, false);
    }

    private SVGProperties getSVGPropertiesFromAttributes(final Attributes pAttributes, final boolean pDeepCopy) {
        if (this.mSVGGroupStack.size() > 0) {
            return new SVGProperties(this.mSVGGroupStack.peek().getSVGProperties(), pAttributes, pDeepCopy);
        } else {
            return new SVGProperties(null, pAttributes, pDeepCopy);
        }
    }

    protected abstract boolean pushTransform(Attributes pAttributes);

    protected abstract void popTransform();

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public abstract RectF getComputedBounds();
}