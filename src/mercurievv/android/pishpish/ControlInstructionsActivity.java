package mercurievv.android.pishpish;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import com.google.inject.Inject;
import mercurievv.android.pishpish.services.GuiceGameActivity;
import mercurievv.android.pishpish.services.MetricsService;
import mercurievv.android.pishpish.services.ResourcesService;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.modifier.RotationByModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.SVGBitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureBuilder;
import org.anddev.andengine.opengl.texture.atlas.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.HorizontalAlign;

import java.util.List;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.8.6
 * Time: 22:11
 */
public class ControlInstructionsActivity extends GuiceGameActivity implements Scene.IOnSceneTouchListener {
    @Inject
    private Engine engine;
    @Inject
    private Scene scene;
    @Inject
    private Camera camera;
    @Inject
    private ResourcesService resourcesService;
    @Inject
    private MetricsService metricsService;

    private List<IShape> shapes = new Vector<IShape>();
    private Font font;
    private int touchCounter = -1;
    private static final String PITCH_SPRITE = "pitch.svg";
    private static final String ROLL_SPRITE = "roll.svg";


    @Override
    public Engine onLoadEngine() {
        return engine;
    }

    @Override
    public void onLoadResources() {
        SVGBitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        BuildableBitmapTextureAtlas textureAtlasObjects = new BuildableBitmapTextureAtlas(1024, 1024, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        float scale = metricsService.getScale();
        resourcesService.loadTexture(textureAtlasObjects, ROLL_SPRITE, scale);
        resourcesService.loadTexture(textureAtlasObjects, PITCH_SPRITE, 2 * scale);
        BitmapTextureAtlas fontTextureAtlas = new BitmapTextureAtlas(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        font = new Font(fontTextureAtlas, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32 * scale, true, Color.BLACK);
        try {
            textureAtlasObjects.build(new BlackPawnTextureBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(1));
        } catch (final ITextureBuilder.TextureAtlasSourcePackingException e) {
            Log.e("ControlInstructions", "Err", e);
        }

        engine.getTextureManager().loadTextures(textureAtlasObjects, fontTextureAtlas);
        engine.getFontManager().loadFont(font);
    }

    @Override
    public Scene onLoadScene() {
        scene = new Scene();

        scene.setBackground(new ColorBackground(0.7f, 0.7f, 0.9f));

        scene.setOnSceneTouchListener(this);

        IShape shape;
        shape = new Text(0, 0, font, "To move, \n tilt \n phone/pad", HorizontalAlign.CENTER);
        useShapeInDemoCycle(shape);

        TextureRegion textureRegion = (TextureRegion) resourcesService.findTextureByPath(ROLL_SPRITE);
        shape = new Sprite(0, 0, textureRegion);
        useShapeInDemoCycle(shape);

        shape = new Text(0, 0, font, "Or like that", HorizontalAlign.CENTER);
        useShapeInDemoCycle(shape);

        textureRegion = (TextureRegion) resourcesService.findTextureByPath(PITCH_SPRITE);
        shape = new Sprite(0, 0, textureRegion);
        useShapeInDemoCycle(shape);

        TouchEvent pSceneTouchEvent = new TouchEvent() {
            @Override
            public boolean isActionDown() {
                return true;
            }
        };
        onSceneTouchEvent(scene, pSceneTouchEvent);

        return scene;
    }

    private void useShapeInDemoCycle(IShape text) {
        shapes.add(text);
        centerSprite(text);
    }

    private void centerSprite(IShape sprite) {
        float x = camera.getCenterX() - sprite.getWidth() / 2;
        float y = camera.getCenterY() - sprite.getHeight() / 2;
        sprite.setPosition(x, y);
    }

    @Override
    public void onLoadComplete() {
    }

    //todo remove multitouch
    @Override
    public boolean onSceneTouchEvent(Scene scene, TouchEvent pSceneTouchEvent) {
        if (!pSceneTouchEvent.isActionDown())
            return false;
        if (isTouchCounterOk())
            scene.detachChild(shapes.get(touchCounter));

        touchCounter++;

        if (isTouchCounterOk()) {
            IShape shape = shapes.get(touchCounter);
            scene.attachChild(shape);

            if (shape instanceof Sprite) {
                int rotation = 75;
                float phaseTiming = 0.5f;
                shape.registerEntityModifier(
                        new SequenceEntityModifier(
                                new RotationByModifier(phaseTiming, rotation)
                                , new RotationByModifier(phaseTiming * 2, -rotation * 2)
                                , new RotationByModifier(phaseTiming, rotation))
                );
            }
        }
        if (shapes.size() == touchCounter) {
            Intent intent = new Intent(this, MyGameActivity.class);
            startActivity(intent);
        }
        return true;
    }

    private boolean isTouchCounterOk() {
        return touchCounter >= 0 && touchCounter < shapes.size();
    }
}
