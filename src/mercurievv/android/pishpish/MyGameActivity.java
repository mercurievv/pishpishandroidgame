package mercurievv.android.pishpish;

import android.view.KeyEvent;
import com.google.inject.Inject;
import com.google.inject.Injector;
import mercurievv.android.pishpish.entities.Player;
import mercurievv.android.pishpish.services.GameLevel;
import mercurievv.android.pishpish.services.GuiceGameActivity;
import mercurievv.android.pishpish.services.SceneContainerService;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.scene.Scene;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;

/**
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga
 *
 * @author Nicolas Gramlich
 * @since 13:58:12 - 21.05.2011
 */
public class MyGameActivity extends GuiceGameActivity {
    public static final int VECTOR_CAMERA_HEIGHT = 240;
    @Inject
    private GameLevel gameLevel;
    @Inject
    private SceneContainerService sceneContainerService;
    @Inject
    private Camera camera;
    @Inject
    private Engine engine;
    @Inject
    private Injector injector;

    private static int baseEntityHeight;
    private static float scale;

    //qvga (240 x 320) = 60
    @Deprecated //use getScale()
    public static int getBaseEntityHeight() {
        return baseEntityHeight;
    }

    //todo use MetricsService
    @Deprecated
    public static float getScale() {
        return scale;
    }

    @Override
    public Engine onLoadEngine() {
        baseEntityHeight = (int) (camera.getHeight() / 4);
        scale = camera.getHeight() / VECTOR_CAMERA_HEIGHT;
        return engine;
    }

    @Override
    public void onLoadResources() {
        gameLevel.loadLevel();
    }

    @Override
    public Scene onLoadScene() {
        return gameLevel.loadScene();
    }

    @Override
    public void onLoadComplete() {
        gameLevel.startGame();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Player player = getPlayerIfPossible();
        if (player == null)
            return true;

        if (keyCode == 21)
            player.moveUp();
        else if (keyCode == 19)
            player.moveRight();
        else if (keyCode == 22)
            player.moveDown();
        else if (keyCode == 20)
            player.moveLeft();
        return super.onKeyDown(keyCode, event);
    }

    private Player getPlayerIfPossible() {
        if (sceneContainerService == null)
            return null;
        return sceneContainerService.getPlayer();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Player player = getPlayerIfPossible();
        if (player == null)
            return true;
        if (keyCode == 19 || keyCode == 20)
            player.stop();
        return super.onKeyUp(keyCode, event);
    }

    public void inject(Object object) {
        RoboInjector injector = RoboGuice.getInjector(this);
        injector.injectMembers(object);
    }
}