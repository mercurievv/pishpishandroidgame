package mercurievv.android.pishpish;

import com.google.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.27.4
 * Time: 20:21
 */
@Singleton
public class GlobalVariables {
    private MyGameActivity myGameActivity;

    public GlobalVariables(MyGameActivity myGameActivity) {
        this.myGameActivity = myGameActivity;
    }
}
