package mercurievv.android.pishpish;

import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.2.4
 * Time: 03:44
 */
public class MultiTextureRegion extends TiledTextureRegion {
    public MultiTextureRegion(final ITexture pTexture, final int pTexturePositionX, final int pTexturePositionY, final int pWidth, final int pHeight, final int pTileColumns, final int pTileRows) {
        super(pTexture, 0, 0, pWidth, pHeight, 0, 0);
    }
}
