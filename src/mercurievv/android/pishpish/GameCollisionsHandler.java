package mercurievv.android.pishpish;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import mercurievv.android.pishpish.services.SceneContainerService;
import mercurievv.android.pishpish.services.injuring.CollisionsProcessingService;
import org.anddev.andengine.engine.handler.IUpdateHandler;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.22.3
 * Time: 00:23
 */
@Singleton
public class GameCollisionsHandler implements IUpdateHandler {
    @Inject
    private SceneContainerService sceneContainerService;
    @Inject
    private CollisionsProcessingService collisionsProcessingService;

    @Override
    public void onUpdate(float pSecondsElapsed) {
        processCollisions();
    }

    @SuppressWarnings("ForLoopReplaceableByForEach")
    private void processCollisions() {
        List<? extends GameObject> injuringObjects = sceneContainerService.getAllGameObjects();
        List<? extends GameObject> liveObjects = sceneContainerService.getAllGameObjects();
        for (int i = 0; i < injuringObjects.size(); i++) { //need to do this (not foreach) because some obj can disappear during this cycles
            GameObject injuringObject = injuringObjects.get(i);
            for (int i1 = 0; i1 < liveObjects.size(); i1++) {
                GameObject liveObject = liveObjects.get(i1);
                collisionsProcessingService.processInjuring(injuringObject, liveObject);
            }
        }
    }

    @Override
    public void reset() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
