package mercurievv.android.pishpish.entities;

import com.google.inject.Inject;
import com.google.inject.Injector;
import mercurievv.android.pishpish.entities.modifiers.JumpableGravityUpdateHandler;
import mercurievv.android.pishpish.entities.modifiers.LandscapeEnvelopingHandler;
import mercurievv.android.pishpish.entities.modifiers.MovePlayerHandler;
import mercurievv.android.pishpish.entities.statuses.Moving;
import mercurievv.android.pishpish.entities.weapon.Lightning;
import mercurievv.android.pishpish.services.SceneContainerService;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.BaseSprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.21.3
 * Time: 22:56
 */
public class Player extends LiveGameObject implements Moving, Scene.IOnSceneTouchListener {

    public static final int ANIMATION_DURATION = 300;
    public static final String WIZARD_SPRITE = "wizard-layers-legless.svg";
    public Direction direction = Direction.STOPPED;
    private MovePlayerHandler movePlayerHandler;
    @Inject
    private Camera camera;
    @Inject
    private SceneContainerService sceneContainerService;
    @Inject
    private LandscapeEnvelopingHandler landscapeEnvelopingHandler;
    @Inject
    private Injector injector;
    private JumpableGravityUpdateHandler gravityModifier;

    @Override
    public void initObject() {
        super.initObject();
        BaseSprite shape = (BaseSprite) getShape();
        movePlayerHandler = new MovePlayerHandler(this);
        stopAnimation();
        shape.setFlippedHorizontal(true);
        gravityModifier = new JumpableGravityUpdateHandler(shape);
        enableGravityModifier(gravityModifier);
        shape.registerUpdateHandler(movePlayerHandler);
        landscapeEnvelopingHandler.setShape(shape);
        shape.registerUpdateHandler(landscapeEnvelopingHandler);
        shape.registerUpdateHandler(new CameraFollowHandler(shape));
        shape.setPosition(0, camera.getHeight());
    }

    @Override
    public void moveUp() {
        jump();
    }

    public void jump() {
        IShape shape = getShape();
        gravityModifier.jump(shape);
    }

    @Override
    public void moveRight() {
        movePlayerHandler.moveRight();
        startAnimateWalking();
    }

    @Override
    public void moveDown() {
    }

    @Override
    public void moveLeft() {
        movePlayerHandler.moveLeft();
        startAnimateWalking();
    }

    @Override
    public void stop() {
        movePlayerHandler.stop();
        stopAnimation();
    }

    @Override
    public Direction getDirection() {
        return direction;
    }

    @Override
    public OwnerSide getSide() {
        return OwnerSide.WE;
    }

    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        return fire(pSceneTouchEvent);
    }

    @Override
    public void endDying() {
        super.endDying();
        sceneContainerService.restartLevel();
    }

    private boolean fire(TouchEvent pSceneTouchEvent) {
        if (!pSceneTouchEvent.isActionDown())
            return true;
        if (!isLive())
            return true;
        if (!isActive())
            return true;
        IShape shape = getShape();
        float startX = shape.getX();
        float startY = shape.getY();
        float finishX = pSceneTouchEvent.getX();
        float finishY = pSceneTouchEvent.getY();
        Lightning lightning = new Lightning();
        injector.injectMembers(lightning);
        sceneContainerService.addGameObject(lightning);
        lightning.throwIt(startX + shape.getWidth(), startY + shape.getHeight() / 2, finishX, finishY);
        soundsProcessor.play(this, "shoot");
        return true;
    }

    @Override
    protected String getShapeId() {
        return WIZARD_SPRITE;
    }

    private void stopAnimation() {
        AnimatedSprite sprite = (AnimatedSprite) getShape();
        sprite.stopAnimation();
    }

    private void startAnimateWalking() {
        AnimatedSprite sprite = (AnimatedSprite) getShape();
        if (sprite.isAnimationRunning())
            return;
        sprite.animate(ANIMATION_DURATION, true);
    }

    @SuppressWarnings("NonStaticInnerClassInSecureContext")
    private class CameraFollowHandler implements IUpdateHandler {
        private final IShape shape;

        public CameraFollowHandler(IShape shape) {
            this.shape = shape;

        }

        @Override
        public void onUpdate(float pSecondsElapsed) {
            float cameraCenterX = shape.getX() + camera.getWidth() / 2 - (shape.getWidth() / 2);
            float cameraCenterY = camera.getCenterY();
            camera.setCenter(cameraCenterX, cameraCenterY);
        }

        @Override
        public void reset() {
            // To change body of implemented methods use File | Settings | File Templates.
        }
    }

    public static class GameAccelerometerListener implements IAccelerometerListener {
        private Player player;

        private float mAccel; // acceleration apart from gravity
        private float mAccelCurrent; // current acceleration including gravity
        private float mAccelLast; // last acceleration including gravity

        public GameAccelerometerListener(Player player) {
            this.player = player;
        }

        @Override
        public void onAccelerometerChanged(AccelerometerData accelerometerData) {
            float x = accelerometerData.getX();
            float y = accelerometerData.getY();
            float z = accelerometerData.getZ();

            walk(x);

            jump(x, y, z);
        }

        private void jump(float x, float y, float z) {
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta; // perform low-cut filter
            if (mAccel > 2)
                player.jump();
        }

        private void walk(float x) {
            x = (x / 10);
            x = Math.max(x, -0.5f);
            x = Math.min(x, 0.5f);
            player.movePlayerHandler.move(x * 2);
        }
    }
}
