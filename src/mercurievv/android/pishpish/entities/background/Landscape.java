package mercurievv.android.pishpish.entities.background;

import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.SimpleGameObject;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.27.3
 * Time: 07:45
 */
public class Landscape extends SimpleGameObject {
    @Inject
    private Camera camera;

    @Override
    protected String getShapeId() {
        return "landscape-l1-gfx.svg";
    }


    @Override
    protected Sprite createCustomSprite(float startX, float startY, TextureRegion textureRegion) {
        return new Sprite(0, camera.getHeight() - textureRegion.getHeight(), textureRegion);
    }
}
