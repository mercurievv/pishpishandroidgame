package mercurievv.android.pishpish.entities.background;

import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.SimpleGameObject;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.3.4
 * Time: 01:36
 */
public class BackgroundObject extends SimpleGameObject {
    @Inject
    private Camera camera;

    private final String path;

    @Override
    protected String getShapeId() {
        return path;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public BackgroundObject(float startX, String path) {
        super(startX, 0);
        this.path = path;
    }

    @Override
    public IShape createShape() {
        IShape shape = super.createShape();
        float x = shape.getX();
        @SuppressWarnings("UnsecureRandomNumberGeneration")
        float y = camera.getHeight() * 0.96f - shape.getHeight();
        shape.setPosition(x, y);
        return shape;
    }
}
