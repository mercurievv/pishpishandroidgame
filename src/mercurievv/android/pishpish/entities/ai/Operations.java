package mercurievv.android.pishpish.entities.ai;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import mercurievv.andengine.ShapesUtils;
import mercurievv.android.pishpish.entities.Boots;
import mercurievv.android.pishpish.entities.LiveGameObject;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.entities.statuses.Live;
import mercurievv.android.pishpish.entities.statuses.Moving;
import mercurievv.android.pishpish.services.MetricsService;
import mercurievv.android.pishpish.services.SVGSizeService;
import mercurievv.android.pishpish.services.SceneContainerService;
import org.anddev.andengine.entity.shape.IShape;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.5.7
 * Time: 22:26
 */
@Singleton
public class Operations {
    private ShapesUtils shapesUtils;
    private MetricsService metricsService;

    @Inject
    public Operations(ShapesUtils shapesUtils, MetricsService metricsService) {
        this.shapesUtils = shapesUtils;
        this.metricsService = metricsService;
    }

    public Injuring getCloseThreat(LiveGameObject object, List<GameObject> possibleThreats) {
        IShape shape = object.getShape();
        Live.OwnerSide objectSide = object.getSide();
        float nearDistance = 100 * metricsService.getScale();
        Injuring closeThreat = null;
        for (GameObject possibleThreat : possibleThreats) {
            if (!(possibleThreat instanceof Injuring))
                continue;
            Injuring threat = (Injuring) possibleThreat;
            boolean isFriend = threat.getOwnerSide() == objectSide;
            if (isFriend)
                continue;
            if (!shapesUtils.isShapesNear(shape, threat.getShape(), nearDistance))
                continue;
            if(closeThreat == null) {
                closeThreat = threat;
            } else {
                if(isNewTheratWorseThanOld(closeThreat, threat))
                    closeThreat = threat;
            }
        }
        return closeThreat;
    }

    private boolean isNewTheratWorseThanOld(Injuring oldThreat, Injuring newThreat) {
        float damageDelta = newThreat.getInjure() - oldThreat.getInjure();
        return (damageDelta > 0);
    }

    public void walkAway(Boots walker, GameObject from) {
        Moving.Direction oppositeDirection = shapesUtils.getOppositeDirection(walker.getShape(), from.getShape());
        moveTo(walker, oppositeDirection);
    }

    private void moveTo(Moving walker, Moving.Direction newDirection) {
        boolean alreadyCorrectDirection = walker.getDirection() == newDirection;
        if(alreadyCorrectDirection)
            return;
        if(newDirection == Moving.Direction.LEFT)
            walker.moveLeft();
        else if (newDirection == Moving.Direction.RIGHT)
            walker.moveRight();
    }

    public void walkAwayFromSceneBorder(SceneContainerService sceneContainerService, Boots boots) {
        float x = boots.getShape().getX();
        SVGSizeService.Rect sceneBounds = sceneContainerService.getSceneBounds();
        float distToLeftBorder = sceneBounds.x - x;
        float distToRightBorder = sceneBounds.x + sceneBounds.width - x;
        Moving.Direction newDirection = distToLeftBorder < distToRightBorder ? Moving.Direction.RIGHT : Moving.Direction.LEFT;
        moveTo(boots, newDirection);
    }

    public boolean canHitEnemy(LiveGameObject enemy) {
        return false;  //To change body of created methods use File | Settings | File Templates.
    }

    public void goToEnemy(LiveGameObject enemy) {
        //To change body of created methods use File | Settings | File Templates.
    }

    @SuppressWarnings("RedundantIfStatement")
    public boolean nearToSceneBorder(SceneContainerService sceneContainerService, IShape shape) {
        SVGSizeService.Rect sceneBounds = sceneContainerService.getSceneBounds();
        float x = shape.getX();
        if(x + shape.getWidth() > sceneBounds.x + sceneBounds.width)
            return true;
        if(x < sceneBounds.x)
            return true;
        return false;
    }

    public void walkForward() {
        //To change body of created methods use File | Settings | File Templates.
    }
}
