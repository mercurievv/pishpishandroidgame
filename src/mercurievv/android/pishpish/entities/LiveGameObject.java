package mercurievv.android.pishpish.entities;

import android.content.Context;
import android.graphics.Path;
import com.google.inject.Inject;
import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.entities.modifiers.GravityUpdateHandler;
import mercurievv.android.pishpish.entities.statuses.Live;
import mercurievv.android.pishpish.services.SoundsProcessor;
import mercurievv.android.pishpish.services.injuring.modifiers.DeathModifierListener;
import org.anddev.andengine.entity.modifier.AlphaModifier;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.RotationByModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.23.3
 * Time: 00:42
 */
public abstract class LiveGameObject extends SimpleGameObject implements Live, Activeable {
    @Inject
    protected SoundsProcessor soundsProcessor;
    @Inject
    private Context context;

    private GravityUpdateHandler gravityUpdateHandler;

    private boolean isLive;
    protected float lifePower;
    protected boolean isActive = true;

    public LiveGameObject() {
        super();
        lifePower = 100;
        isLive = true;
    }

    public LiveGameObject(Path placeOnMap) {
        super(placeOnMap);
        lifePower = 100;
        isLive = true;
    }

    public LiveGameObject(float startX, float startY) {
        super(startX, startY);
        lifePower = 100;
        isLive = true;
    }

    public static String shapeToString(IShape liveShape) {
        return " x:" + liveShape.getX() + " y:" + liveShape.getY() + " w:" + liveShape.getWidth() + " h:" + liveShape.getHeight();
    }

    @Override
    public void startDying(IEntityModifier deathModifier) {
        isLive = false;
        sceneContainerService.ungroupGameObject(this);

        visualizeDeath(deathModifier);
    }

    @Override
    public void endDying() {
        IShape shape = getShape();
        scene.detachChild(shape);
    }

    @Override
    public boolean isLive() {
        return isLive;
    }

    @Override
    public void resurrect() {
        isLive = true;
    }

    @Override
    public void injure(float injure, final IEntityModifier entityModifier) {
        lifePower -= injure;
        if (lifePower <= 0) {
            SequenceEntityModifier deathModifier = new SequenceEntityModifier(
                    new DeathModifierListener(this, "hit.ogg"),
                    new RotationByModifier(0.5f, 900f),
                    new AlphaModifier(2f, 1f, 0f)
            );
            startDying(deathModifier);
            return;
        }
        getShape().registerEntityModifier(entityModifier);
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void activate() {
        this.isActive = true;
    }

    @Override
    public void deactivate() {
        this.isActive = false;
    }

    @Override
    public void sound(String file) {
        soundsProcessor.play(this, file);
    }

    private void visualizeDeath(IEntityModifier deathModifier) {
        IShape sprite = getShape();
        sprite.clearEntityModifiers();
        sprite.clearUpdateHandlers();
        sprite.registerEntityModifier(deathModifier);
    }

    protected void enableGravityModifier() {
        enableGravityModifier(new GravityUpdateHandler(getShape()));
    }

    protected void enableGravityModifier(GravityUpdateHandler gravityUpdateHandler1) {
        if (gravityUpdateHandler == null) {
            gravityUpdateHandler = gravityUpdateHandler1;
            ((MyGameActivity) context).inject(gravityUpdateHandler);
        }
        getShape().registerUpdateHandler(gravityUpdateHandler);
    }
}
