package mercurievv.android.pishpish.entities;

import android.graphics.Path;
import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.statuses.NotItersectable;
import mercurievv.android.pishpish.services.LandscapeEnvelopeContainerService;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.11.6
 * Time: 00:28
 */
@NotItersectable
public class Wall extends SimpleGameObject {
    @Inject
    private LandscapeEnvelopeContainerService landscapeEnvelopeContainerService;

    public static final String WALL_SPRITE = "wall.svg";

    @SuppressWarnings("UnusedDeclaration")
    public Wall(Path placeOnMap) {
        super(placeOnMap);
    }

    public Wall(float startX, float startY) {
        super(startX, startY);
    }

    @Override
    protected String getShapeId() {
        return WALL_SPRITE;
    }

    @Override
    protected IShape createShape() {
        IShape shape = super.createShape();
        float x = shape.getX();
        float y = landscapeEnvelopeContainerService.getFloorYByX(x) - shape.getHeight();
        shape.setPosition(x, y);
        return shape;
    }
}
