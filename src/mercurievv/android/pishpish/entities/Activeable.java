package mercurievv.android.pishpish.entities;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.4.5
 * Time: 22:55
 */
public interface Activeable {
    boolean isActive();

    void activate();

    void deactivate();
}
