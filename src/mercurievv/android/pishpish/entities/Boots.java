package mercurievv.android.pishpish.entities;

import android.graphics.Path;
import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.ai.Operations;
import mercurievv.android.pishpish.entities.modifiers.InertionMoving;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.entities.statuses.Moving;
import mercurievv.android.pishpish.entities.statuses.MovingState;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.LoopEntityModifier;
import org.anddev.andengine.entity.modifier.MoveByModifier;
import org.anddev.andengine.entity.modifier.ParallelEntityModifier;
import org.anddev.andengine.entity.modifier.RotationByModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.BaseSprite;
import org.anddev.andengine.util.Debug;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.29.3
 * Time: 19:56
 */
//todo implement injuring
public class Boots extends LiveGameObject implements Moving {

    @Inject
    private Operations ops;
    @Inject
    private InertionMoving inertionMoving;

    private Moving moving = new MovingState();
    private IEntityModifier moveRotationModifier;

    @Deprecated //for tests only
    public Boots(float startX, float startY) {
        super(startX, startY);
    }

    public Boots(Path placeOnMap) {
        super(placeOnMap);
    }

    @Override
    public OwnerSide getSide() {
        return OwnerSide.THEY;
    }

    @Override
    protected String getShapeId() {
        return "boots.svg";
    }

    @Override
    public void initObject() {
        super.initObject();
        final IShape sprite = getShape();
        enableGravityModifier();
        sprite.registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                //survive block
                Boots object = Boots.this;
                Injuring closeThreat = ops.getCloseThreat(object, sceneContainerService.getAllGameObjects());
                if(closeThreat != null) {
                    ops.walkAway(object, closeThreat);
                    return;
                }
                //enemy hunt block
/*
                Player player = sceneContainerService.getPlayer();
                if(ops.canHitEnemy(player)){
                    ops.goToEnemy(player);
                    return;
                }
*/
                //obstructions block
                if(ops.nearToSceneBorder(sceneContainerService, sprite)) {
                    ops.walkAwayFromSceneBorder(sceneContainerService, object);
                }
                ops.walkForward();
            }

            @Override
            public void reset() {

            }
        });
        moveRight();
    }

    @Override
    public void moveRight() {
        moveTo(Direction.RIGHT);
        moving.moveRight();
    }

    @Override
    public void moveLeft() {
        moveTo(Direction.LEFT);
        moving.moveLeft();
    }

    private void moveTo(Direction newDirection) {
        Debug.d("1" + this + " " + newDirection.name());
        IShape shape = getShape();
        shape.unregisterEntityModifier(moveRotationModifier);
        SequenceEntityModifier sequenceEntityModifier = new SequenceEntityModifier(new MoveByModifier(0.01f, 0.01f, 0));
        //sequenceEntityModifier = flipIfNeeded(sequenceEntityModifier, shape, newDirection);
        sequenceEntityModifier = walk(sequenceEntityModifier, newDirection);
        moveRotationModifier = sequenceEntityModifier;
        shape.registerEntityModifier(sequenceEntityModifier);
    }

    SequenceEntityModifier walk(SequenceEntityModifier sequenceEntityModifier, Direction newDirection) {
        if(getDirection() != newDirection) {
            Debug.d("2" + this + " " + newDirection.name());
            float horizontalSpeed = 50 * metricsService.getScale();
            LoopEntityModifier moveModifier = new LoopEntityModifier(new MoveByModifier(1f, horizontalSpeed * newDirection.getxFactor(), 0f));
            LoopEntityModifier rotationModifier = new LoopEntityModifier(new RotationByModifier(0.5f, 90f * newDirection.getxFactor()));

            moveRotationModifier = new ParallelEntityModifier(moveModifier, rotationModifier);
            sequenceEntityModifier = new SequenceEntityModifier(sequenceEntityModifier, moveRotationModifier);
        }
        return sequenceEntityModifier;
    }

    SequenceEntityModifier flipIfNeeded(SequenceEntityModifier sequenceEntityModifier, IShape shape, Direction newDirection) {
        boolean directionToRight = ((BaseSprite) shape).isFlippedHorizontal();
        boolean needToSwitchDirection = directionToRight != (newDirection == Direction.RIGHT);
        if(needToSwitchDirection) {
            float newWidthFactor = newDirection.getxFactor();
            float width = shape.getWidth() * newWidthFactor;
            float height = shape.getHeight();
            ScaleModifier flipModifier = new ScaleModifier(2f, width, -width, height, height);
            sequenceEntityModifier = new SequenceEntityModifier(sequenceEntityModifier, flipModifier);
        }
        return sequenceEntityModifier;
    }

    @Override
    public void moveUp() {
    }

    @Override
    public void moveDown() {
    }

    @Override
    public void stop() {
        moving.stop();
    }

    @Override
    public Direction getDirection() {
        return moving.getDirection();
    }
}
