package mercurievv.android.pishpish.entities;

import android.graphics.Path;
import com.google.inject.Inject;
import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.entities.modifiers.LandscapeEnvelopingHandler;
import mercurievv.android.pishpish.entities.modifiers.LiveObjectEmergenceModifier;
import mercurievv.android.pishpish.entities.modifiers.ParticleEmergenceModifier;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.modifier.LoopEntityModifier;
import org.anddev.andengine.entity.modifier.MoveByModifier;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.26.3
 * Time: 02:02
 */
public class Shorty extends LiveGameObject implements Injuring {
    public static final String SHORTY_SPRITE = "shorty.svg";
    @Inject
    private Camera camera;
    @Inject
    private LandscapeEnvelopingHandler landscapeEnvelopingHandler;
    private LiveObjectEmergenceModifier emergenceModifier;

    @SuppressWarnings("UnusedDeclaration")
    @Deprecated
//for tests only
    Shorty(float startX, float startY) {
        super(startX, startY);
    }

    @SuppressWarnings("UnusedDeclaration")
    public Shorty(Path path) {
        super(path);
        lifePower = 40;
    }

    @Override
    public void initObject() {
        super.initObject();
        //putOnGround();
        IShape shape = getShape();
        enableGravityModifier();
        landscapeEnvelopingHandler.setShape(shape);
        shape.registerUpdateHandler(landscapeEnvelopingHandler);
        BaseTextureRegion textureRegion = resourcesService.findTextureByPath("particle_point.png");
        emergenceModifier = new ParticleEmergenceModifier(5f, this, camera, (TextureRegion) textureRegion, scene);
        shape.registerEntityModifier(emergenceModifier);
    }

    @Override
    public OwnerSide getSide() {
        return OwnerSide.THEY;
    }

    @Override
    protected String getShapeId() {
        return SHORTY_SPRITE;
    }

    @Override
    public float getInjure() {
        return 10;
    }

    @Override
    public void injureWasMade() {
    }

    @Override
    public OwnerSide getOwnerSide() {
        return OwnerSide.THEY;
    }

    @Override
    public void activate() {
        super.activate();
        IShape shape = getShape();
        float horizontalSpeed = MyGameActivity.getBaseEntityHeight() / 8;
        shape.registerEntityModifier(new LoopEntityModifier(new MoveByModifier(1f, -horizontalSpeed, 0f)));
    }

    @Override
    protected AnimatedSprite createCustomAnimatedSprite(float startX, float startY, TiledTextureRegion tiledTextureRegion) {
        AnimatedSprite sprite = new AnimatedSprite(startX, startY, tiledTextureRegion) {
            @Override
            protected void onManagedDraw(GL10 pGL, Camera pCamera) {
                if (emergenceModifier != null && !isActive()) {
                    pGL.glPushMatrix();
                    pGL.glEnable(GL10.GL_SCISSOR_TEST);

                    int[] appearArea = emergenceModifier.getAppearArea();
                    pGL.glScissor(appearArea[0], appearArea[1], appearArea[2], appearArea[3]);
                    super.onManagedDraw(pGL, pCamera);

                    pGL.glDisable(GL10.GL_SCISSOR_TEST);
                    pGL.glPopMatrix();
                } else {
                    super.onManagedDraw(pGL, pCamera);
                }
            }
        };
        sprite.animate(new long[]{200, 200, 200, 200}, new int[]{0, 1, 2, 1}, -1);
        return sprite;
    }
}
