package mercurievv.android.pishpish.entities.statuses;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.21.3
 * Time: 23:51
 */
//todo think to remove all methods. Leave only statuses
public interface Moving extends Status {
    void moveUp();

    void moveRight();

    void moveDown();

    void moveLeft();

    void stop();

    Direction getDirection();

    enum Direction {
        RIGHT(1), LEFT(-1), STOPPED(0);
        private float xFactor;

        Direction(float xFactor) {
            this.xFactor = xFactor;
        }

        public float getxFactor() {
            return xFactor;
        }

        public Direction getOpposite(Direction direction) {
            if(direction == RIGHT)
                return LEFT;
            if(direction == LEFT)
                return RIGHT;
            return direction;
        }
    }
}
