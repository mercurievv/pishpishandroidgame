package mercurievv.android.pishpish.entities.statuses;

import org.anddev.andengine.entity.modifier.IEntityModifier;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.23.3
 * Time: 00:39
 */
public interface Live extends GameObject {
    void startDying(IEntityModifier deathModifier);

    void endDying();

    void injure(float injure, final IEntityModifier entityModifier);

    boolean isLive();

    void resurrect();

    OwnerSide getSide();

    void sound(String file);


    enum OwnerSide {
        WE, THEY, NATURE
    }
}
