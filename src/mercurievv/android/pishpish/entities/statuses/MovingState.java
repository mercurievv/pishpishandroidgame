package mercurievv.android.pishpish.entities.statuses;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.8.7
 * Time: 22:15
 */
public class MovingState implements Moving {
    private Direction direction;

    @Override
    public void moveUp() {
    }

    @Override
    public void moveRight() {
        direction = Direction.RIGHT;
    }

    @Override
    public void moveDown() {
    }

    @Override
    public void moveLeft() {
        direction = Direction.LEFT;
    }

    @Override
    public void stop() {
        direction = Direction.STOPPED;
    }

    @Override
    public Direction getDirection() {
        return direction;
    }
}
