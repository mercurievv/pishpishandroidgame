package mercurievv.android.pishpish.entities.statuses;

import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.24.3
 * Time: 01:17
 */
public interface GameObject {
    IShape getShape();
}
