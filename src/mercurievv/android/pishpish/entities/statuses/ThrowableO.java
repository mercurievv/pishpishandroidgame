package mercurievv.android.pishpish.entities.statuses;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.24.3
 * Time: 01:06
 */
public interface ThrowableO {
    void stop();

    void throwIt(float startX, float startY, float finishX, float finishY);
}
