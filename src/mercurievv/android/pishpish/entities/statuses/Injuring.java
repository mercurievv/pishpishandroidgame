package mercurievv.android.pishpish.entities.statuses;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.24.3
 * Time: 01:11
 */
public interface Injuring extends GameObject {
    float DEADLY_INJURE = Float.MAX_VALUE;
    float getInjure();

    void injureWasMade();

    Live.OwnerSide getOwnerSide();
}
