package mercurievv.android.pishpish.entities.modifiers;

import com.google.inject.Inject;
import mercurievv.android.pishpish.services.LandscapeEnvelopeContainerService;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.4.4
 * Time: 22:21
 */
public class LandscapeEnvelopingHandler implements IUpdateHandler {
    @Inject
    private LandscapeEnvelopeContainerService landscapeEnvelopeContainerService;

    private IShape entity;

    public void setShape(IShape entity) {
        this.entity = entity;
    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
        if (landscapeEnvelopeContainerService.isOverGround(entity))
            return;
        float x = entity.getX();
        float centerX = x + (entity.getWidth() / 2);
        float angle = landscapeEnvelopeContainerService.getAngleByX(centerX);
        angle = Math.min(angle, 15);
        angle = Math.max(angle, -15);
        entity.setRotation(angle);
    }

    @Override
    public void reset() {
    }
}
