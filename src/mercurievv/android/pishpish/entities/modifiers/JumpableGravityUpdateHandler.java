package mercurievv.android.pishpish.entities.modifiers;

import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.8.5
 * Time: 00:58
 */
public class JumpableGravityUpdateHandler extends GravityUpdateHandler {
    public static final int SECONDS_TO_FLY = 2;
    private float remainingJumpSeconds;

    public JumpableGravityUpdateHandler(IShape iEntity) {
        super(iEntity);
    }

    @Override
    protected void modify(float pSecondsElapsed, IShape shape, int verticalSpeed) {
        float x = shape.getX();
        float y = shape.getY();

        if (remainingJumpSeconds > 0) {
            remainingJumpSeconds -= pSecondsElapsed;
            y = y - pSecondsElapsed * verticalSpeed;
            shape.setPosition(x, y);
            return;
        }
        super.modify(pSecondsElapsed, shape, verticalSpeed);
    }

    public void jump(IShape shape) {
        if (!landscapeEnvelopeContainerService.isOverGround(shape))
            remainingJumpSeconds = SECONDS_TO_FLY;
    }
}
