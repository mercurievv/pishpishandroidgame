package mercurievv.android.pishpish.entities.modifiers;

import mercurievv.android.pishpish.entities.LiveGameObject;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.particle.ParticleSystem;
import org.anddev.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.anddev.andengine.entity.particle.initializer.AlphaInitializer;
import org.anddev.andengine.entity.particle.initializer.VelocityInitializer;
import org.anddev.andengine.entity.particle.modifier.AlphaModifier;
import org.anddev.andengine.entity.particle.modifier.ExpireModifier;
import org.anddev.andengine.entity.particle.modifier.ScaleModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.modifier.IModifier;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.4.5
 * Time: 23:53
 */
public class ParticleEmergenceModifier extends LiveObjectEmergenceModifier {
    private ParticleSystem particleSystem;
    private TextureRegion particleTextureRegion;
    private Scene scene;
    private RectangleParticleEmitter particleEmitter;

    public ParticleEmergenceModifier(final float pDuration, final LiveGameObject liveGameObject, final Camera camera, TextureRegion particleTextureRegion, Scene scene) {
        super(pDuration, liveGameObject, camera);
        this.particleTextureRegion = particleTextureRegion;
        this.scene = scene;
        addModifierListener(new ParticleEmergenceModifierListener());
    }

    @Override
    protected void onSetValue(IEntity pItem, float pPercentageDone, float pValue) {
        Shape shape = (Shape) pItem;
        super.onSetValue(shape, pPercentageDone, pValue);
        particleEmitter.setCenter(shape.getX() + shape.getWidth() / 2, (shape.getY() + shape.getHeight()) - pValue);
    }

    @SuppressWarnings("NonStaticInnerClassInSecureContext")
    private class ParticleEmergenceModifierListener extends EmergenceModifierListener {
        @Override
        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            super.onModifierStarted(pModifier, pItem);
            particleEmitter = new RectangleParticleEmitter(pItem.getX(), pItem.getY(), 30, 10);
            particleSystem = new ParticleSystem(particleEmitter, 20, 30, 40, particleTextureRegion); //todo recalculate

            //particleSystem.addParticleInitializer(new ColorInitializer(1, 0, 0));
            particleSystem.addParticleInitializer(new AlphaInitializer(0));
            particleSystem.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE);
            particleSystem.addParticleInitializer(new VelocityInitializer(-4, 4, -25, -9)); //todo recalculate
            //particleSystem.addParticleInitializer(new ScaleInitializer(1));
            //particleSystem.addParticleInitializer(new RotationInitializer(0.0f, 360.0f));

            particleSystem.addParticleModifier(new ScaleModifier(2.0f, 6.0f, 0, 5)); //todo recalculate
//            particleSystem.addParticleModifier(new ColorModifier(1, 1, 0, 0.5f, 0, 0, 0, 3));
//            particleSystem.addParticleModifier(new ColorModifier(1, 1, 0.5f, 1, 0, 1, 4, 6));
            particleSystem.addParticleModifier(new AlphaModifier(0, 1, 0, 1));
            particleSystem.addParticleModifier(new AlphaModifier(1, 0, 2, 3));
            particleSystem.addParticleModifier(new ExpireModifier(2, 3));

            scene.attachChild(particleSystem);
        }

        @Override
        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
            super.onModifierFinished(pModifier, pItem);
            scene.detachChild(particleSystem);
        }
    }
}
