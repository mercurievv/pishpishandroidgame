package mercurievv.android.pishpish.entities.modifiers;

import com.google.inject.Inject;
import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.services.LandscapeEnvelopeContainerService;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.BaseEntityUpdateHandler;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.22.3
 * Time: 13:48
 */
public class GravityUpdateHandler extends BaseEntityUpdateHandler {
    @Inject
    private Camera camera;
    @Inject
    LandscapeEnvelopeContainerService landscapeEnvelopeContainerService;

    protected float secondsElapsed;

    @SuppressWarnings("UnusedDeclaration")
    public GravityUpdateHandler(IShape shape) {
        super(shape);
    }

    @Override
    public void onUpdate(float pSecondsElapsed, IEntity entity) {
        IShape shape = (IShape) entity;
        secondsElapsed += pSecondsElapsed;
        final int verticalSpeed = MyGameActivity.getBaseEntityHeight() / 2;
        modify(pSecondsElapsed, shape, verticalSpeed);
    }

    protected void modify(float pSecondsElapsed, IShape shape, int verticalSpeed) {
        float x = shape.getX();
        float y = shape.getY();

        float groundY = landscapeEnvelopeContainerService.getShapeGroundY(shape);
        if (landscapeEnvelopeContainerService.isOverGround(y, groundY)) {
            y = y + pSecondsElapsed * verticalSpeed;
            shape.setPosition(x, y);
        }
        if (landscapeEnvelopeContainerService.isUnderGround(y, groundY)) {
            y = groundY;
            shape.setPosition(x, y);
        }
    }

    @Override
    public void reset() {
        super.reset();
        this.secondsElapsed = 0;
    }
}
