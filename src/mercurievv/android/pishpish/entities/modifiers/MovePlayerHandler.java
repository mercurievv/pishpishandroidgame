package mercurievv.android.pishpish.entities.modifiers;

import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.entities.Player;
import mercurievv.android.pishpish.entities.statuses.Moving;
import org.anddev.andengine.engine.handler.BaseEntityUpdateHandler;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.22.3
 * Time: 13:48
 */
public class MovePlayerHandler extends BaseEntityUpdateHandler implements IUpdateHandler {
    private Moving.Direction direction = Moving.Direction.STOPPED;
    private float speedByAccelerometer;
    private float pathSinceLastChop;
    private Player player;

    public MovePlayerHandler(Player player) {
        super(player.getShape());
        this.player = player;
    }

    @Override
    public void onUpdate(float pSecondsElapsed, IEntity pItem) {
        float x = pItem.getX();
        float y = pItem.getY();
        float xShift;
        IShape shape = (IShape) pItem;
        boolean controlByAccelerometrInsteadButtons = direction != Moving.Direction.RIGHT
                && direction != Moving.Direction.LEFT;
        if (controlByAccelerometrInsteadButtons) {
            xShift = pSecondsElapsed * speedByAccelerometer;
        } else {
            xShift = pSecondsElapsed * shape.getWidth();
            xShift = direction == Moving.Direction.RIGHT ? xShift : -xShift;
        }
        x = x + xShift;
        chopIfNeed(xShift, shape);
        pItem.setPosition(x, y);
    }

    private void chopIfNeed(float shift, IShape shape) {
        pathSinceLastChop += Math.abs(shift);
        if (pathSinceLastChop <= shape.getWidth() / 4)
            return;
        pathSinceLastChop = 0;
        player.sound("chop");
    }


    public void moveRight() {
        direction = Moving.Direction.RIGHT;
    }

    public void moveLeft() {
        direction = Moving.Direction.LEFT;
    }

    public void stop() {
        direction = Moving.Direction.STOPPED;
    }

    public void move(float speedCoef) {
        speedByAccelerometer = MyGameActivity.getBaseEntityHeight() * speedCoef;
    }
}
