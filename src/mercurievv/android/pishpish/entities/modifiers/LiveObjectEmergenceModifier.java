package mercurievv.android.pishpish.entities.modifiers;

import mercurievv.android.pishpish.entities.LiveGameObject;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.SingleValueSpanEntityModifier;
import org.anddev.andengine.entity.primitive.BaseRectangle;
import org.anddev.andengine.util.modifier.IModifier;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.4.5
 * Time: 19:56
 */
public class LiveObjectEmergenceModifier extends SingleValueSpanEntityModifier {
    private LiveGameObject liveGameObject;
    private Camera camera;
    private int[] appearArea = new int[4];

    /*
    * Create only before LGO added to scene. Because it's desabling only here
    */
    public LiveObjectEmergenceModifier(final float pDuration, final LiveGameObject liveGameObject, final Camera camera) {
        super(pDuration, 0, liveGameObject.getShape().getHeight());
        addModifierListener(new EmergenceModifierListener());
        this.liveGameObject = liveGameObject;
        this.camera = camera;
    }

    public LiveObjectEmergenceModifier(float pDuration, float pFromValue, float pToValue, IEntityModifierListener pEntityModifierListener, LiveGameObject liveGameObject, Camera camera) {
        super(pDuration, pFromValue, pToValue, pEntityModifierListener);
        this.liveGameObject = liveGameObject;
        this.camera = camera;
    }

    public int[] getAppearArea() {
        return appearArea;
    }

    private LiveObjectEmergenceModifier(final LiveObjectEmergenceModifier liveObjectEmergenceModifier) {
        super(liveObjectEmergenceModifier);
        this.camera = liveObjectEmergenceModifier.camera;
    }

    @Override
    protected void onSetValue(IEntity pItem, float pPercentageDone, float pValue) {
        BaseRectangle rectangle = (BaseRectangle) pItem;
        int x = (int) (rectangle.getX() - camera.getMinX());
        int y = (int) ((camera.getMaxY() - rectangle.getY()) - rectangle.getHeight());
        int width = (int) rectangle.getWidth();
        int height = (int) pValue;
        appearArea[0] = x;
        appearArea[1] = y;
        appearArea[2] = width;
        appearArea[3] = height;
    }

    @Override
    public IEntityModifier deepCopy() throws DeepCopyNotSupportedException {
        return new LiveObjectEmergenceModifier(this);
    }

    @Override
    protected void onSetInitialValue(IEntity pItem, float pValue) {
    }

    @SuppressWarnings("NonStaticInnerClassInSecureContext")
    public class EmergenceModifierListener implements IEntityModifierListener {
        @Override
        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            liveGameObject.deactivate();
        }

        @Override
        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
            liveGameObject.activate();
        }
    }
}
