package mercurievv.android.pishpish.entities;

import android.graphics.Path;
import com.google.inject.Inject;
import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.entities.statuses.Live;
import mercurievv.android.pishpish.services.LandscapeEnvelopeContainerService;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.1.5
 * Time: 22:18
 */
public class Hole extends SimpleGameObject implements Injuring {

    @Inject
    private LandscapeEnvelopeContainerService landscapeEnvelopeContainerService;

    @SuppressWarnings("UnusedDeclaration")
    public Hole(Path placeOnMap) {
        super(placeOnMap);
    }

    @Override
    protected IShape createShape() {
        Line pathLine = pathService.getPathLine(placeOnMap, MyGameActivity.getScale());
        float y1 = pathLine.getY1();
        float y2 = pathLine.getY2();
        y1 = landscapeEnvelopeContainerService.convertSvgYToLandscape(y1);
        y2 = landscapeEnvelopeContainerService.convertSvgYToLandscape(y2);
        pathLine.setPosition(pathLine.getX1(), y1, pathLine.getX2(), y2);
        pathLine.setAlpha(0);
        return pathLine;
    }


    @Override
    protected String getShapeId() {
        return null;
    }

    @Override
    public float getInjure() {
        return DEADLY_INJURE;
    }

    @Override
    public void injureWasMade() {
    }

    @Override
    public Live.OwnerSide getOwnerSide() {
        return Live.OwnerSide.NATURE;
    }

}
