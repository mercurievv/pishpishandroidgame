package mercurievv.android.pishpish.entities;

import android.graphics.Path;
import com.google.inject.Inject;
import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import mercurievv.android.pishpish.services.LandscapeEnvelopeContainerService;
import mercurievv.android.pishpish.services.MetricsService;
import mercurievv.android.pishpish.services.PathService;
import mercurievv.android.pishpish.services.ResourcesService;
import mercurievv.android.pishpish.services.SceneContainerService;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.BaseSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.21.3
 * Time: 22:27
 */
public abstract class SimpleGameObject implements GameObject {
    private IShape shape;
    private float startX;
    private float startY;
    @Inject
    protected Scene scene;
    @Inject
    protected ResourcesService resourcesService;
    @Inject
    protected SceneContainerService sceneContainerService;
    @Inject
    protected PathService pathService;
    @Inject
    protected LandscapeEnvelopeContainerService landscapeEnvelopeContainerService;
    @Inject
    protected MetricsService metricsService;
    protected Path placeOnMap;

    public SimpleGameObject(Path placeOnMap) {
        this.placeOnMap = placeOnMap;
    }

    public SimpleGameObject() {
    }

    public SimpleGameObject(float startX, float startY) {
        this.startX = startX;
        this.startY = startY;
    }

    public IShape getShape() {
        return shape;
    }

    //run after all dependencies injected. It's not happen on obj creation, because it isn't DI bean
    public void initObject() {
        if (placeOnMap != null)
            calculateStartPositionFromPath(placeOnMap);
        shape = createShape();
    }

    private void calculateStartPositionFromPath(Path placeOnMap) {
        setStartPositionOnGround(placeOnMap);
    }

    private void setStartPositionOnGround(Path placeOnMap) {
        float[] pathMiddlePoint = pathService.getPathMiddlePoint(placeOnMap, MyGameActivity.getScale());
        startX = pathMiddlePoint[PathService.X_INDEX];
        startY = landscapeEnvelopeContainerService.getFloorYByX(startX);
    }

    protected IShape createShape() {
        BaseTextureRegion baseTextureRegion = resourcesService.findTextureByPath(getShapeId());
        return createSprite(baseTextureRegion, startX, startY);
    }

    protected abstract String getShapeId();

    private BaseSprite createSprite(BaseTextureRegion baseTextureRegion, float startX, float startY) {
        if (baseTextureRegion instanceof TextureRegion) {
            final TextureRegion textureRegion = (TextureRegion) baseTextureRegion;
            Sprite sprite = createCustomSprite(startX, startY, textureRegion);
            //sprite.setColor(0, 0, 0);
            return sprite;
        } else if (baseTextureRegion instanceof TiledTextureRegion) {
            final TiledTextureRegion tiledTextureRegion = (TiledTextureRegion) baseTextureRegion;
            final AnimatedSprite animatedSprite = createCustomAnimatedSprite(startX, startY, tiledTextureRegion);
            //animatedSprite.setColor(0, 0, 0); //need to make objects transparent
            animatedSprite.animate(100);
            return animatedSprite;
        }
        return null;
    }

    //todo createCustomSprite and createCustomAnimatedSprite can be merged in one method
    protected Sprite createCustomSprite(float startX, float startY, TextureRegion textureRegion) {
        return new Sprite(startX, startY, textureRegion);
    }

    protected AnimatedSprite createCustomAnimatedSprite(float startX, float startY, TiledTextureRegion tiledTextureRegion) {
        return new AnimatedSprite(startX, startY, tiledTextureRegion);
    }
}
