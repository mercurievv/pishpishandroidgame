package mercurievv.android.pishpish.entities.weapon;

import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.entities.SimpleGameObject;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.entities.statuses.Live;
import mercurievv.android.pishpish.entities.statuses.ThrowableO;
import org.anddev.andengine.entity.modifier.LoopEntityModifier;
import org.anddev.andengine.entity.modifier.MoveByModifier;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.24.3
 * Time: 01:32
 */
public class Lightning extends SimpleGameObject implements Injuring, ThrowableO, GameObject {

    public void throwIt(float startX, float startY, float finishX, float finishY) {
        IShape shape = getShape();
        shape.setPosition(startX, startY);
        //setPath(startX, startY, finishX, finishY);
        finishX = finishX - (shape.getWidth() / 2);
        finishY = finishY - (shape.getHeight() / 2);
        float vectorX = finishX - startX;
        float vectorY = finishY - startY;
        float pace = (float) Math.sqrt(vectorX * vectorX + vectorY * vectorY);
        shape.registerEntityModifier(new LoopEntityModifier(new MoveByModifier(pace / (MyGameActivity.getBaseEntityHeight() * 2), vectorX, vectorY)));
    }

    @Override
    public float getInjure() {
        return 15;
    }

    @Override
    public void injureWasMade() {
        stop();
    }

    @Override
    public Live.OwnerSide getOwnerSide() {
        return Live.OwnerSide.WE;
    }

    @Override
    public void stop() {
        sceneContainerService.removeGameObject(this);
    }

    @Override
    protected String getShapeId() {
        return "lightning.svg";
    }
}
