package mercurievv.android.pishpish.services;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Picture;
import android.util.Log;
import com.google.inject.Inject;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.extension.svg.PictureSVGHandlerImpl;
import org.anddev.andengine.extension.svg.SVGParser;
import org.anddev.andengine.extension.svg.adt.ISVGColorMapper;
import org.anddev.andengine.extension.svg.adt.SVG;
import org.anddev.andengine.extension.svg.adt.SVGDirectColorMapper;
import org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.SVGBitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.source.SVGBaseBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.region.BaseTextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import roboguice.inject.ContextSingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.23.3
 * Time: 02:03
 */
@ContextSingleton
public class ResourcesService {
    @Inject
    private MetricsService metricsService;
    private final Map<String, BaseTextureRegion> texturesNames = new HashMap<String, BaseTextureRegion>();
    protected Context context;
    protected Engine engine;
    private final ISVGColorMapper colorMapper = new SVGDirectColorMapper();
    private final SVGSizeService svgSizeService;


    @Inject
    public ResourcesService(Context context, Engine engine, SVGSizeService svgSizeService) {
        this.context = context;
        this.engine = engine;
        this.svgSizeService = svgSizeService;
    }

    public BitmapTextureAtlas loadAnimatedTextureFromSetOfFiles(String name, String... paths) {
        String assetBasePath = SVGBitmapTextureAtlasTextureRegionFactory.getAssetBasePath();
        SVGSizeService.Rect rect = svgSizeService.parseSize(context, assetBasePath + paths[0]);
        int width = (int) rect.width;
        int height = (int) rect.height;

        int frames = paths.length;
        BitmapTextureAtlas textureAtlas = new BitmapTextureAtlas(getPowerOf2More(width * frames)
                , getPowerOf2More(height)
                , TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        for (int i = 0; i < frames; i++) {
            String path = paths[i];
            SVGBitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, context, path, width, height, colorMapper, width * i, 0);
        }
        TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(textureAtlas, 0, 0, width * frames, height, frames, 1);
        getTexturesNames().put(name, tiledTextureRegion);
        return textureAtlas;
    }

    public BitmapTextureAtlas loadAnimatedTexture(String path) {
        String assetBasePath = SVGBitmapTextureAtlasTextureRegionFactory.getAssetBasePath();
        SVGSizeService.Rect rect = svgSizeService.parseSize(context, assetBasePath + path);
        int width = (int) rect.width;
        int height = (int) rect.height;

        PictureSVGHandlerImpl svgHandler = new PictureSVGHandlerImpl(colorMapper);

        try {
            SVGParser.parseSVGFromAsset(context.getAssets(), SVGBitmapTextureAtlasTextureRegionFactory.getAssetBasePath() + path, svgHandler);
        } catch (IOException e) {
            Log.e("Resource", "Error loading animated sprite", e);
        }
        List<Picture> pictures = svgHandler.getPictures();
        int frames = pictures.size();
        BitmapTextureAtlas textureAtlas = new BitmapTextureAtlas(getPowerOf2More(width * frames)
                , getPowerOf2More(height)
                , TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        for (int i = 0, picturesSize = pictures.size(); i < picturesSize; i++) {
            Picture picture = pictures.get(i);
            SVGBaseBitmapTextureAtlasSource atlasSource = new SVGBaseBitmapTextureAtlasSource(new SVG(picture, svgHandler.getBounds(), svgHandler.getComputedBounds()), width, height);
            TextureRegionFactory.createFromSource(textureAtlas, atlasSource, width * i, 0, false);
        }
        TiledTextureRegion tiledTextureRegion = new TiledTextureRegion(textureAtlas, 0, 0, width * frames, height, frames, 1);
        getTexturesNames().put(path, tiledTextureRegion);
        return textureAtlas;
    }

    public int getPowerOf2More(double number) {
        int res = 2;
        while (res < number) {
            res *= 2;
        }
        return res;
    }

    public TextureRegion loadTexture(BuildableBitmapTextureAtlas mBuildableBitmapTextureAtlas, String path) {
        return loadTexture(mBuildableBitmapTextureAtlas, path, metricsService.getScale());
    }

    public TextureRegion loadTexture(BuildableBitmapTextureAtlas textureAtlasObjects, String path, float scale) {
        TextureRegion textureRegion = SVGBitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlasObjects, context, path, scale, colorMapper);
        getTexturesNames().put(path, textureRegion);
        return textureRegion;
    }

    @SuppressWarnings("UnusedDeclaration")
    private TiledTextureRegion loadTexture(Context activity, BuildableBitmapTextureAtlas mBuildableBitmapTextureAtlas, String path, int columns, int rows, int width, int height) {
        TiledTextureRegion textureRegion = SVGBitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mBuildableBitmapTextureAtlas, activity, path, width, height, colorMapper, columns, rows);
        getTexturesNames().put(path, textureRegion);
        return textureRegion;
    }

    public BaseTextureRegion findTextureByPath(String path) {
        BaseTextureRegion baseTextureRegion = getTexturesNames().get(path);
        if (baseTextureRegion == null)
            Log.e("Resources", "Cannot find texture. Probably another Guice context.", new RuntimeException("Cannot find texture named '" + path + "' in textures map: " + getTexturesNames().toString()));
        return baseTextureRegion;
    }

    public void loadTextures(ITexture... textures) {
        engine.getTextureManager().loadTextures(textures);
    }

    public Map<String, BaseTextureRegion> getTexturesNames() {
        return texturesNames;
    }

    private static class GrayscaleColorMapper implements ISVGColorMapper {
        @Override
        public Integer mapColor(Integer pColor) {
            if (pColor == null) {
                return null;
            } else {
                int red = Color.red(pColor);
                int green = Color.green(pColor);
                int blue = Color.blue(pColor);
                int grayscale = (red + green + blue) / 3;
                return Color.argb(Color.alpha(pColor), grayscale, grayscale, grayscale);
            }
        }
    }
}
