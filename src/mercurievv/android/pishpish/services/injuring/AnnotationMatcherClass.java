package mercurievv.android.pishpish.services.injuring;

import mercurievv.android.pishpish.entities.statuses.GameObject;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.12.6
 * Time: 01:02
 */
public class AnnotationMatcherClass extends ClassCollisionMatcher<GameObject, GameObject> {

    private final Class class1;
    private final Class class2;

    public AnnotationMatcherClass(Class class1, Class class2, CollisionAction<GameObject, GameObject> collisionAction) {
        super(GameObject.class, GameObject.class, collisionAction);
        this.class1 = class1;
        this.class2 = class2;
    }

    @Override
    public boolean isApplyable(Object firstObject, Object secondObject) {
        if (!isObjectMatched(firstObject, class1))
            return false;
        if (!isObjectMatched(secondObject, class2))
            return false;

        performAction(firstClass.cast(firstObject), secondClass.cast(secondObject));
        return true;
    }

    private boolean isObjectMatched(Object object, Class aClass) {
        if (aClass == null)
            return true;
        //noinspection unchecked
        if (aClass.isAnnotation() && object.getClass().isAnnotationPresent(aClass))
            return true;
        else if (aClass.isInstance(object))
            return true;
        return false;
    }
}
