package mercurievv.android.pishpish.services.injuring;

import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.Hole;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.entities.statuses.Live;
import mercurievv.android.pishpish.entities.statuses.NotItersectable;
import mercurievv.android.pishpish.services.injuring.modifiers.HoleFallingAction;
import mercurievv.android.pishpish.services.injuring.modifiers.SimplyInjuringAction;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.shape.IShape;
import roboguice.inject.ContextSingleton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.11.5
 * Time: 04:12
 */
@ContextSingleton
public class CollisionsProcessingService {
    private List<CollisionMatcher<? extends GameObject, ? extends GameObject>> collisionMatchers = new ArrayList<CollisionMatcher<? extends GameObject, ? extends GameObject>>();

    @Inject
    public CollisionsProcessingService(
            SimplyInjuringAction simplyInjuringAction,
            HoleFallingAction holeFallingAction
    ) {
        collisionMatchers.add(new ClassCollisionMatcher<Injuring, Live>(Hole.class, Live.class, holeFallingAction));
        collisionMatchers.add(new ClassCollisionMatcher<Injuring, Live>(Injuring.class, Live.class, simplyInjuringAction));
        collisionMatchers.add(new AnnotationMatcherClass(NotItersectable.class, Live.class, new RedoCollision()));
    }

    public void processInjuring(GameObject object1, GameObject object2) {
        if (isObjectsCollide(object1, object2))
            for (CollisionMatcher<? extends GameObject, ? extends GameObject> collisionMatcher : collisionMatchers) {
                if (collisionMatcher.isApplyable(object1, object2)) {
                    return;
                }
            }
    }

    private boolean isObjectsCollide(GameObject object1, GameObject object2) {
        IShape liveObjectShape = object1.getShape();
        IShape byInjuringObjectShape = object2.getShape();
        if (!byInjuringObjectShape.collidesWith(liveObjectShape))
            return false;
        if (byInjuringObjectShape instanceof Line) //for objects like @link Hole
            if (!isOverLine((Line) byInjuringObjectShape, liveObjectShape))
                return false;
        return true;
    }

    private boolean isOverLine(Line line, IShape liveObjectShape) {
        float lineX1 = Math.min(line.getX1(), line.getX2());
        float lineX2 = Math.max(line.getX1(), line.getX2());
        float objectX1 = liveObjectShape.getX();
        float objectX2 = objectX1 + liveObjectShape.getHeightScaled() / 2;
        return (lineX1 <= objectX2 && objectX2 <= lineX2);
    }

}
