package mercurievv.android.pishpish.services.injuring;

import mercurievv.android.pishpish.entities.statuses.GameObject;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.11.6
 * Time: 23:53
 */
public class ClassCollisionMatcher<F extends GameObject, S extends GameObject> implements CollisionMatcher<F, S> {
    protected final Class<? extends F> firstClass;
    protected final Class<? extends S> secondClass;
    protected final CollisionAction<F, S> collisionAction;

    public ClassCollisionMatcher(Class<? extends F> firstClass, Class<? extends S> secondClass, CollisionAction<F, S> collisionAction) {
        this.firstClass = firstClass;
        this.secondClass = secondClass;
        this.collisionAction = collisionAction;
    }

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean isApplyable(Object firstObject, Object secondObject) {
        if (!isMatch(firstObject, firstClass))
            return false;
        if (!isMatch(secondObject, secondClass))
            return false;
        performAction(firstClass.cast(firstObject), secondClass.cast(secondObject));
        return true;
    }

    private boolean isMatch(Object object, Class aClass) {
        return aClass.isInstance(object);
    }

    @Override
    public void performAction(F firstObject, S secondObject) {
        collisionAction.performAction(firstObject, secondObject);
    }
}
