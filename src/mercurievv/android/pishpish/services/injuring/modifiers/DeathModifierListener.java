package mercurievv.android.pishpish.services.injuring.modifiers;

import mercurievv.android.pishpish.entities.statuses.Live;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.util.modifier.IModifier;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.5.5
 * Time: 16:18
 */
@SuppressWarnings("NonStaticInnerClassInSecureContext")
public class DeathModifierListener extends SoundingModifierListener {

    public DeathModifierListener(Live live, String soundFile) {
        super(live, soundFile);
    }

    @Override
    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
        super.onModifierStarted(pModifier, pItem);
    }

    @Override
    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
        live.endDying();
    }
}
