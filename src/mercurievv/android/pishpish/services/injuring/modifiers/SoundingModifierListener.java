package mercurievv.android.pishpish.services.injuring.modifiers;

import mercurievv.android.pishpish.entities.statuses.Live;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.IModifier;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.28.5
 * Time: 11:33
 */
public class SoundingModifierListener implements IEntityModifier.IEntityModifierListener {
    protected Live live;
    protected final String soundFile;

    public SoundingModifierListener(Live live, String soundFile) {
        this.live = live;
        this.soundFile = soundFile;
    }

    @Override
    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
        if (soundFile != null && !soundFile.equals(""))
            live.sound(soundFile);

    }

    @Override
    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
    }
}
