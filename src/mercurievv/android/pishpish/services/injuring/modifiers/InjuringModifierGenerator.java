package mercurievv.android.pishpish.services.injuring.modifiers;

import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.entities.statuses.Live;
import org.anddev.andengine.entity.modifier.IEntityModifier;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.25.5
 * Time: 20:19
 */
public interface InjuringModifierGenerator {
    IEntityModifier createModifier(Injuring injuring, Live live);
}
