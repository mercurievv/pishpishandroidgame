package mercurievv.android.pishpish.services.injuring.modifiers;

import android.util.Log;
import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.LiveGameObject;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.entities.statuses.Live;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.modifier.MoveModifier;
import org.anddev.andengine.entity.modifier.ParallelEntityModifier;
import org.anddev.andengine.entity.modifier.RotationByModifier;
import org.anddev.andengine.entity.modifier.ScaleModifier;
import org.anddev.andengine.entity.shape.IShape;
import roboguice.inject.ContextSingleton;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.27.5
 * Time: 19:35
 */
@ContextSingleton
public class HoleFallingAction extends SimplyInjuringAction {
    @Inject
    private Camera camera;

    @Override
    public void performAction(Injuring injuring, Live live) {
        if (!isInjured(injuring, live))
            return;

        IShape injuringShape = injuring.getShape();
        IShape liveShape = live.getShape();
        if (!isLiveInsideInjuring(injuringShape, liveShape))
            return;
        Log.i("FallInHole", "Falling object :" + LiveGameObject.shapeToString(liveShape)
                + " \nto hole :" + LiveGameObject.shapeToString(injuringShape));

        float duration = 2f;
        DeathModifierListener deathModifierListener = new DeathModifierListener(live, "falling");
        ParallelEntityModifier entityModifier = new ParallelEntityModifier(
                new RotationByModifier(duration, -360 * 2, deathModifierListener)
                , new ScaleModifier(duration, 1f, 0f)
                , new MoveModifier(duration, liveShape.getX(), liveShape.getX(), liveShape.getY(), camera.getHeight())
        );

        live.startDying(entityModifier);
    }

    boolean isLiveInsideInjuring(IShape injuringShape, IShape liveShape) {
        float liveCenterX = liveShape.getX() + liveShape.getWidth() / 2;
        float minX = Math.min(injuringShape.getX(), injuringShape.getX() + injuringShape.getWidth());
        float maxX = Math.max(injuringShape.getX(), injuringShape.getX() + injuringShape.getWidth());
        return liveCenterX > minX && liveCenterX < maxX;
    }

}
