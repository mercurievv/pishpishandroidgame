package mercurievv.android.pishpish.services.injuring.modifiers;

import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.Activeable;
import mercurievv.android.pishpish.entities.Player;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.entities.statuses.Live;
import mercurievv.android.pishpish.services.injuring.CollisionAction;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.modifier.MoveByModifier;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.entity.shape.Shape;
import roboguice.inject.ContextSingleton;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.25.5
 * Time: 20:23
 */
@ContextSingleton
public class SimplyInjuringAction implements CollisionAction<Injuring, Live> {
    @Inject
    private Camera camera;

    @Override
    public void performAction(Injuring injuring, Live live) {
        if (!isInjured(injuring, live))
            return;
        float injure = injuring.getInjure();

        IShape liveShape = live.getShape();
        double moveY = liveShape.getWidth() * 2;
        if (live instanceof Player)
            moveY *= -1;
        MoveByModifier hit = new MoveByModifier(0.5f, (float) moveY, 0f, new SoundingModifierListener(live, "hit"));

        live.injure(injure, hit);
        injuring.injureWasMade();
    }


    @SuppressWarnings({"RedundantIfStatement"})
    protected boolean isInjured(Injuring byInjuringObject, Live liveObject) {
        if (byInjuringObject.getOwnerSide().equals(liveObject.getSide()))
            return false;
        if (!liveObject.isLive())
            return false;
        if (byInjuringObject instanceof Live && !((Live) byInjuringObject).isLive())
            return false;

        if (!isObjectActive(liveObject))
            return false;
        if (!isObjectActive(byInjuringObject))
            return false;

        return true;
    }

    private boolean isObjectActive(Object object) {
        if (object instanceof Activeable) {
            Activeable activeable = (Activeable) object;
            return activeable.isActive();
        }
        return true;
    }

    private boolean isShapeVisibleByCamera(Shape shape) {
        if (shape instanceof RectangularShape)
            return camera.isRectangularShapeVisible((RectangularShape) shape);
        else if (shape instanceof Line)
            return camera.isLineVisible((Line) shape);
        throw new RuntimeException("Cannot calculate object visibility for type: " + shape.getClass().getName());
    }


}
