package mercurievv.android.pishpish.services.injuring;

import mercurievv.android.pishpish.entities.statuses.GameObject;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.12.6
 * Time: 01:08
 */
public interface CollisionAction<F extends GameObject, S extends GameObject> {
    void performAction(F object1, S object2);
}
