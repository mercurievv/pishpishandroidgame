package mercurievv.android.pishpish.services.injuring;

import mercurievv.android.pishpish.entities.statuses.GameObject;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.25.5
 * Time: 09:35
 */
public interface CollisionMatcher<F extends GameObject, S extends GameObject> {
    boolean isApplyable(Object firstObject, Object secondObject);

    void performAction(F firstObject, S secondObject);
}
