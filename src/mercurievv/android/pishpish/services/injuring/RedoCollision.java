package mercurievv.android.pishpish.services.injuring;

import mercurievv.android.pishpish.entities.statuses.GameObject;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.12.6
 * Time: 18:19
 */
class RedoCollision implements CollisionAction<GameObject, GameObject> {
    @Override
    public void performAction(GameObject nonIntersectable, GameObject object2) {
        IShape obj2Shape = object2.getShape();
        IShape nonIntersShape = nonIntersectable.getShape();

        float difY = getShapeCenterY(obj2Shape) - getShapeCenterY(nonIntersShape);
        float shiftY = difY < 0 ? -obj2Shape.getHeight() : nonIntersShape.getHeight();
        float newY = nonIntersShape.getY() + shiftY;

        float difX = getShapeCenterX(obj2Shape) - getShapeCenterX(nonIntersShape);
        float shiftX = difX < 0 ? -obj2Shape.getWidth() : nonIntersShape.getWidth();
        float newX = nonIntersShape.getX() + shiftX;

        if (Math.abs(newX - obj2Shape.getX()) >= Math.abs(newY - obj2Shape.getY())) {
            obj2Shape.setPosition(obj2Shape.getX(), newY);
        } else {
            obj2Shape.setPosition(newX, obj2Shape.getY());
        }
    }

    private float getShapeCenterX(IShape shape) {
        return shape.getX() + shape.getHeight() / 2;
    }

    private float getShapeCenterY(IShape shape) {
        return shape.getY() + shape.getWidth() / 2;
    }
}
