package mercurievv.android.pishpish.services;

import android.content.Context;
import android.util.Log;
import com.google.inject.Inject;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.util.Debug;
import roboguice.inject.ContextSingleton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.11.5
 * Time: 00:31
 */
//ogg only!!!!
@ContextSingleton
public class SoundsContainer {
    @Inject
    private Engine engine;
    @Inject
    private Context context;
    private Map<String, SoundsGroup> sounds = new HashMap<String, SoundsGroup>();

    public SoundsContainer() {
        SoundFactory.setAssetBasePath("sounds/");
    }

    public void loadSound(String name) {
        try {
            Sound sound = SoundFactory.createSoundFromAsset(engine.getSoundManager(), context, name + ".ogg");
            String groupName = getGroupName(name);
            SoundsGroup soundsGroup = sounds.get(groupName);
            if (soundsGroup == null) {
                soundsGroup = new SoundsGroup();
                sounds.put(groupName, soundsGroup);
            }
            soundsGroup.add(sound);
        } catch (final IOException e) {
            Debug.e(e);
        }
    }

    private String getGroupName(String name) {
        return name.replaceAll("\\-\\d*", "");
    }

    //play group
    public void play(String filename) {
        play(filename, 1f, 1f);
    }

    public void play(String filename, float leftVolume, float rightVolume) {
        if (!isSoundExist(filename)) {
            Log.e("SOUND_CONTAINER", "Sound '" + filename + "' try to be played. Bat is was not loaded (resources)");
            return;
        }
        SoundsGroup soundsGroup = sounds.get(filename);
        Sound sound = soundsGroup.next();
        sound.play();
        sound.setVolume(leftVolume, rightVolume);
    }

    //check group
    public boolean isSoundExist(String filename) {
        return sounds.get(filename) != null;
    }

    private static class SoundsGroup {
        private List<Sound> list = new ArrayList<Sound>();
        private int counter;

        public void add(Sound sound) {
            list.add(sound);
        }

        public Sound next() {
            Sound sound = list.get(counter);
            counter++;
            if (counter >= list.size())
                counter = 0;
            return sound;
        }
    }
}
