package mercurievv.android.pishpish.services;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import com.google.inject.Singleton;
import org.anddev.andengine.extension.svg.SVGParser;
import org.anddev.andengine.extension.svg.exception.SVGParseException;
import org.anddev.andengine.extension.svg.util.SAXHelper;
import org.anddev.andengine.extension.svg.util.constants.ISVGConstants;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.5.6
 * Time: 23:28
 */
@Singleton
public class SVGSizeService {

    public Rect parseSize(Context context, String filename) {
        AssetManager assets = context.getAssets();
        SVGSizeHandler svgHandler = new SVGSizeHandler();
        try {
            SVGParser.parseSVGFromAsset(assets, filename, svgHandler);
        } catch (SVGParseException e) {
            Throwable cause = e.getCause();
            if ("Break".equals(cause.getMessage())) {
                return svgHandler.getSize();
            }
            Log.d("SVGSizeService", "Can't parse svg", e);
        } catch (IOException e) {
            Log.d("SVGSizeService", "Can't parse svg because IO", e);
        }
        return svgHandler.getSize();
    }

    private static class SVGSizeHandler extends DefaultHandler {
        private Rect rect;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            localName = qName;
            if (!localName.equals(ISVGConstants.TAG_SVG))
                return;
            final float x = (float) Math.floor(SAXHelper.getFloatAttribute(attributes, ISVGConstants.ATTRIBUTE_X, 0f));
            final float y = (float) Math.floor(SAXHelper.getFloatAttribute(attributes, ISVGConstants.ATTRIBUTE_Y, 0f));
            final float width = (float) Math.ceil(SAXHelper.getFloatAttribute(attributes, ISVGConstants.ATTRIBUTE_WIDTH, 0f));
            final float height = (float) Math.ceil(SAXHelper.getFloatAttribute(attributes, ISVGConstants.ATTRIBUTE_HEIGHT, 0f));
            rect = new Rect(x, y, width, height);
            Log.i("SVGSize", "x=" + x + ";y=" + y + ";width=" + width + ";height=" + height);
            throw new SAXException("Break");
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);
        }

        public Rect getSize() {
            return rect;
        }
    }

    private static class BreakParsingException extends RuntimeException {
    }

    public static class Rect {
        public float x;
        public float y;
        public float width;
        public float height;

        public Rect(float x, float y, float width, float height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }
}
