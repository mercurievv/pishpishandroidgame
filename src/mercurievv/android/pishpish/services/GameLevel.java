package mercurievv.android.pishpish.services;

import android.content.Context;
import android.util.Log;
import com.google.inject.Inject;
import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.entities.Player;
import mercurievv.android.pishpish.entities.Shorty;
import mercurievv.android.pishpish.entities.Wall;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.SVGBitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.AssetBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.buildable.BuildableTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureBuilder;
import org.anddev.andengine.opengl.texture.atlas.buildable.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.Debug;
import roboguice.inject.ContextSingleton;

import java.io.IOException;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.5.6
 * Time: 19:24
 */
@ContextSingleton
public class GameLevel {
    @Inject
    private Scene scene;
    @Inject
    private SceneContainerService sceneContainerService;
    @Inject
    private MetricsService metricsService;

    private Music mMusic;
    private final Context context;
    private final Engine engine;
    private SoundsContainer soundsContainer;
    private final ResourcesService resourcesService;

    @Inject
    public GameLevel(Context context, Engine engine, SoundsContainer soundsContainer, ResourcesService resourcesService) {
        this.context = context;
        this.engine = engine;
        this.soundsContainer = soundsContainer;
        this.resourcesService = resourcesService;
    }

    public void loadLevel() {
        long t = new Date().getTime();
        SVGBitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        loadTextures();
        loadAudioData();
        Log.i("Levels", "Level resources loading took ms: " + String.valueOf(new Date().getTime() - t));
    }

    public Scene loadScene() {
        return sceneContainerService.initGameScene();
    }

    public void startGame() {
        mMusic.play();
    }

    private void loadTextures() {
        final int height = MyGameActivity.getBaseEntityHeight();

        BuildableBitmapTextureAtlas textureAtlasObjects = new BuildableBitmapTextureAtlas(1024, 1024, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BitmapTextureAtlas wizardTextureAtlas = resourcesService.loadAnimatedTexture(Player.WIZARD_SPRITE);
        BitmapTextureAtlas shortyTextureAtlas = resourcesService.loadAnimatedTexture(Shorty.SHORTY_SPRITE);
        resourcesService.loadTexture(textureAtlasObjects, "lightning.svg");
        resourcesService.loadTexture(textureAtlasObjects, "boots.svg");
        resourcesService.loadTexture(textureAtlasObjects, "level1-bgr.svg");
        resourcesService.loadTexture(textureAtlasObjects, "tree1.svg");
        resourcesService.loadTexture(textureAtlasObjects, "tree2.svg");
        resourcesService.loadTexture(textureAtlasObjects, "tree3.svg");
        resourcesService.loadTexture(textureAtlasObjects, "bush1.svg");
        resourcesService.loadTexture(textureAtlasObjects, "sign.svg");
        resourcesService.loadTexture(textureAtlasObjects, Wall.WALL_SPRITE);

        TextureRegion textureRegion = BuildableTextureAtlasTextureRegionFactory.createFromSource(textureAtlasObjects, new AssetBitmapTextureAtlasSource(context, "gfx/particle_point.png"), true);
        resourcesService.getTexturesNames().put("particle_point.png", textureRegion);

        BuildableBitmapTextureAtlas textureAtlLandscape = initLandscape();

        try {
            textureAtlasObjects.build(new BlackPawnTextureBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(1));
            textureAtlLandscape.build(new BlackPawnTextureBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0));
        } catch (final ITextureBuilder.TextureAtlasSourcePackingException e) {
            Debug.e(e);
        }


        resourcesService.loadTextures(
                textureAtlLandscape,
                textureAtlasObjects,
                shortyTextureAtlas,
                wizardTextureAtlas
        );
    }

    private void loadAudioData() {
        MusicFactory.setAssetBasePath("music/");
        try {
            mMusic = MusicFactory.createMusicFromAsset(engine.getMusicManager(), context, "music.ogg");
            mMusic.setLooping(true);

            soundsContainer.loadSound("chop-1");
            soundsContainer.loadSound("chop-2");
            soundsContainer.loadSound("falling");
            soundsContainer.loadSound("hit");
            soundsContainer.loadSound("shoot");
        } catch (final IOException e) {
            Debug.e(e);
        }
    }

    private BuildableBitmapTextureAtlas initLandscape() {
        float scale = metricsService.getScale();
        double landscapeW = 2262 * scale; //todo use width/height from svg
        int landscapeTextureW = resourcesService.getPowerOf2More(landscapeW);
        int landscapeTextureH = resourcesService.getPowerOf2More((double) 37 * scale);
        BuildableBitmapTextureAtlas textureAtlLandscape = new BuildableBitmapTextureAtlas(landscapeTextureW, landscapeTextureH, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        resourcesService.loadTexture(textureAtlLandscape, "landscape-l1-gfx.svg");
        return textureAtlLandscape;
    }
}
