package mercurievv.android.pishpish.services.guice;

import android.util.DisplayMetrics;
import com.google.inject.Inject;
import com.google.inject.Provider;
import mercurievv.android.pishpish.services.GuiceGameActivity;
import org.anddev.andengine.engine.camera.Camera;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.23.3
 * Time: 02:23
 */
public class CameraProvider implements Provider<Camera> {
    @Inject
    private DisplayMetrics displayMetrics;
    @Override
    public Camera get() {
        //todo use MetricsService
        int width = Math.max(GuiceGameActivity.metrics.widthPixels, GuiceGameActivity.metrics.heightPixels);
        int height = Math.min(GuiceGameActivity.metrics.widthPixels, GuiceGameActivity.metrics.heightPixels);
        return new Camera(0, 0, width, height);
    }
}
