package mercurievv.android.pishpish.services.guice;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.25.4
 * Time: 13:36
 */
public class EngineProvider implements Provider<Engine> {
    @Inject
    private Camera camera;

    @Override
    public Engine get() {
        return new Engine(
                new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new FillResolutionPolicy(), camera)
                        .setNeedsMusic(true)
                        .setNeedsSound(true));
    }
}
