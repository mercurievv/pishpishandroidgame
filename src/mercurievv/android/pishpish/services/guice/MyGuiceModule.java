package mercurievv.android.pishpish.services.guice;

import android.graphics.PathMeasure;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.scene.Scene;

@SuppressWarnings("UnusedDeclaration")
public class MyGuiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Camera.class).toProvider(new CameraProvider()).in(Singleton.class);
        bind(Engine.class).toProvider(new EngineProvider()).in(Singleton.class);
        bind(Scene.class).in(Singleton.class);
        bind(PathMeasure.class).in(Singleton.class);
        //bind(Player.class).in(Singleton.class);
        //bind(MyGameActivity.class).in(Singleton.class);
    }
}