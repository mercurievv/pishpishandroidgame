package mercurievv.android.pishpish.services;

import com.google.inject.Inject;
import org.anddev.andengine.engine.camera.Camera;
import roboguice.inject.ContextSingleton;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.9.6
 * Time: 02:21
 */
@ContextSingleton
public class MetricsService {
    public static final int VECTOR_CAMERA_HEIGHT = 240;
    @Inject
    private Camera camera;

    public float getScale() {
        return camera.getHeight() / VECTOR_CAMERA_HEIGHT;
    }
}
