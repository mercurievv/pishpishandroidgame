package mercurievv.android.pishpish.services;

import android.content.Context;
import android.graphics.Path;
import android.util.Log;
import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.Boots;
import mercurievv.android.pishpish.entities.Hole;
import mercurievv.android.pishpish.entities.LiveGameObject;
import mercurievv.android.pishpish.entities.Shorty;
import mercurievv.android.pishpish.entities.SimpleGameObject;
import mercurievv.android.pishpish.entities.Wall;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.extension.svg.PathSVGHandlerImpl;
import roboguice.inject.ContextSingleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.27.4
 * Time: 17:26
 */
@ContextSingleton
public class LevelObjectsMapService {
    @Inject
    private PathService pathService;
    @Inject
    private SceneContainerService sceneContainerService;
    @Inject
    private LandscapeEnvelopeContainerService landscapeEnvelopeContainerService;
    @Inject
    private Camera camera;
    @Inject
    private Scene scene;
    @Inject
    private Context context;

    public void loadMapFile() {
        PathSVGHandlerImpl pathSVGHandler = pathService.loadPaths(context, "gfx/landscape-l1.svg");
        loadLandscape(pathSVGHandler);
        loadEntities(pathSVGHandler);
    }

    private void loadLandscape(PathSVGHandlerImpl pathSVGHandler) {
        List<Path> landscapePaths = pathSVGHandler.getPaths("Landscape");
        double spriteHeight = pathSVGHandler.getHeight();
        landscapeEnvelopeContainerService.loadLandscape(landscapePaths, spriteHeight);
        sceneContainerService.setSceneBounds(new SVGSizeService.Rect(0, 0, (float)pathSVGHandler.getWidth(), (float) spriteHeight));
    }

    private void loadEntities(PathSVGHandlerImpl pathSVGHandler) {
        loadObjectsGroup(pathSVGHandler, Wall.class);
        loadObjectsGroup(pathSVGHandler, Hole.class);
        loadObjectsGenerator(pathSVGHandler, Shorty.class);
        loadObjectsGenerator(pathSVGHandler, Boots.class);
    }

    private void loadObjectsGenerator(PathSVGHandlerImpl pathSVGHandler, Class<? extends SimpleGameObject> simpleGameObjectClass) {
        String simpleClassName = simpleGameObjectClass.getSimpleName();
        List<Path> paths = pathSVGHandler.getPaths(simpleClassName + "Generator");
        for (Path path : paths) {
            SOGeneratorTimerEvent timerEvent = new SOGeneratorTimerEvent(path, simpleGameObjectClass);
            TimerHandler timerHandler = new TimerHandler(20, true, timerEvent);
            scene.registerUpdateHandler(timerHandler); //probably can be problem when level ends and need unregister this handlers
        }

    }

    private void loadObjectsGroup(PathSVGHandlerImpl pathSVGHandler, Class<? extends SimpleGameObject> aClass) {
        List<Path> paths = pathSVGHandler.getPaths(aClass.getSimpleName());
        for (Path path : paths) {
            SimpleGameObject simpleGameObject = createSGO(aClass, path);
            sceneContainerService.addGameObject(simpleGameObject);
            Log.d("Map", "Loaded object by class " + aClass + " " + LiveGameObject.shapeToString(simpleGameObject.getShape()));
        }
    }

    private <S extends SimpleGameObject> S createSGO(Class<S> aClass, Path path) {
        try {
            Constructor<S> constructor = aClass.getConstructor(Path.class);
            return constructor.newInstance(path);
        } catch (InstantiationException e) {
            Log.e("Map", "Cannot create map object", e);
        } catch (IllegalAccessException e) {
            Log.e("Map", "Cannot create map object", e);
        } catch (InvocationTargetException e) {
            Log.e("Map", "Cannot create map object", e);
        } catch (NoSuchMethodException e) {
            Log.e("Map", "Class " + aClass.getCanonicalName() + " should contain constructor with Path parameter.", e);
        }
        return null;
    }

    @SuppressWarnings("NonStaticInnerClassInSecureContext")
    private class SOGeneratorTimerEvent implements ITimerCallback {
        private Class<? extends SimpleGameObject> simpleGameObjectClass;
        private Path path;

        private SOGeneratorTimerEvent(Path path, Class<? extends SimpleGameObject> simpleGameObjectClass) {
            this.path = path;
            this.simpleGameObjectClass = simpleGameObjectClass;
        }

        @Override
        public void onTimePassed(TimerHandler pTimerHandler) {
            SimpleGameObject object = createSGO(simpleGameObjectClass, path);
            sceneContainerService.addGameObject(object);
        }
    }

}
