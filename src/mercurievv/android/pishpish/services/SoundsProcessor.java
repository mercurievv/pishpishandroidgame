package mercurievv.android.pishpish.services;

import android.util.Log;
import com.google.inject.Inject;
import mercurievv.android.pishpish.entities.LiveGameObject;
import mercurievv.android.pishpish.entities.statuses.Live;
import mercurievv.android.pishpish.entities.statuses.Sounding;
import org.anddev.andengine.engine.camera.Camera;
import roboguice.inject.ContextSingleton;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.28.5
 * Time: 19:47
 */
@ContextSingleton
public class SoundsProcessor {
    @Inject
    private Camera camera;
    @Inject
    private SoundsContainer soundsContainer;

    public void play(LiveGameObject liveGameObject, String filename) {
        String soundsGroup = getSoundType(liveGameObject);
        if (soundsContainer.isSoundExist(soundsGroup + filename)) {
            sound(soundsGroup + filename, liveGameObject);
        } else {
            sound(filename, liveGameObject);
        }
    }

    private void sound(String filename, LiveGameObject liveGameObject) {
        float cameraCenterX = camera.getCenterX();
        float objectX = liveGameObject.getShape().getX();
        float distanceToSoundObject = objectX - cameraCenterX;


        float cameraWidth = camera.getWidth();
        float hearingDistance = cameraWidth * 1.5f;
        float someVakue = distanceToSoundObject / hearingDistance;
        float leftRightDiff = 0.25f;
        Log.i("Sound", someVakue + " : " + leftRightDiff);
        float volRight = 1 - Math.abs(someVakue + leftRightDiff);
        float volLeft = 1 - Math.abs(someVakue - leftRightDiff);
        Log.i("Sound", volLeft + " " + volRight);
        soundsContainer.play(filename, volLeft, volRight);
    }

    private String getSoundType(Live live) {
        Sounding sounding = live.getClass().getAnnotation(Sounding.class);
        if (sounding == null)
            return "";
        String type = sounding.type();
        if (type == null)
            return "";
        return type + "/";
    }
}
