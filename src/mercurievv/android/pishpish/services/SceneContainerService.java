package mercurievv.android.pishpish.services;

import android.content.Context;
import com.google.inject.Inject;
import mercurievv.andengine.AutoCameraParallax;
import mercurievv.android.pishpish.GameCollisionsHandler;
import mercurievv.android.pishpish.MyGameActivity;
import mercurievv.android.pishpish.entities.Player;
import mercurievv.android.pishpish.entities.SimpleGameObject;
import mercurievv.android.pishpish.entities.background.BackgroundObject;
import mercurievv.android.pishpish.entities.background.Landscape;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.sensor.SensorDelay;
import org.anddev.andengine.sensor.accelerometer.AccelerometerSensorOptions;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import roboguice.inject.ContextSingleton;

import java.util.LinkedList;
import java.util.List;

import static mercurievv.andengine.AutoCameraParallax.ParallaxEntity;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.23.3
 * Time: 03:26
 */
@ContextSingleton
public class SceneContainerService {
    @Inject
    private ResourcesService resourcesService;
    @Inject
    private Scene scene;
    //by using MyGameActivity guice freeze. Don't know why yet. SO, better to inject context and then cast.
    @Inject
    private Context context;
    @Inject
    private PathService pathService;
    @Inject
    private LevelObjectsMapService levelObjectsMapService;
    @Inject
    private MetricsService metricsService;
    @Inject
    private GameCollisionsHandler gameCollisionsHandler;


    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private List<GameObject> allGameObjects = new LinkedList<GameObject>();
    private Player player;
    private SVGSizeService.Rect sceneBounds;

    public Scene initGameScene() {
        //engine.registerUpdateHandler(new FPSLogger());
        restartLevel();

        return scene;
    }

    public void restartLevel() {
        cleanScene();

        loadObjectsFromMap();

        initBackground2ndLayer();

        initPlayer();

        initBackground1stLayer();

        scene.registerUpdateHandler(gameCollisionsHandler);
        scene.setOnSceneTouchListener(player);

        Player.GameAccelerometerListener gameAccelerometerListener = new Player.GameAccelerometerListener(player);
        ((BaseGameActivity) context).enableAccelerometerSensor(gameAccelerometerListener, new AccelerometerSensorOptions(SensorDelay.GAME));

    }

    private void cleanScene() {
        allGameObjects.clear();
        scene.reset();
        ((BaseGameActivity) context).disableAccelerometerSensor();
    }

    private void initPlayer() {
        player = new Player();
        addGameObject(player);
    }

    private void loadObjectsFromMap() {
        levelObjectsMapService.loadMapFile();
    }

    private void initBackground1stLayer() {
        Landscape landscape = new Landscape();
        addUnregisteredObject(landscape);
    }

    private void initBackground2ndLayer() {
        AutoCameraParallax parallaxBackground = new AutoCameraParallax(0.9f, 0.9f, 0.9f);

        addBackgroundObject(parallaxBackground, new BackgroundObject(0, "level1-bgr.svg"), -8f);
        float distanceBetweenBGObjects = 128 * metricsService.getScale();
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects, "tree2.svg"), -1.5f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 2, "sign.svg"), -1.3f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 3, "tree1.svg"), -2f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 4, "bush1.svg"), -1.5f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 5, "tree3.svg"), -1.1f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 6, "tree2.svg"), -1.5f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 7, "sign.svg"), -1.3f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 8, "tree1.svg"), -2f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 9, "bush1.svg"), -1.5f);
        addBackgroundObject(parallaxBackground, new BackgroundObject(distanceBetweenBGObjects * 10, "tree3.svg"), -1.2f);

        scene.setBackground(parallaxBackground);
    }

    private void addBackgroundObject(AutoCameraParallax parallaxBackground, BackgroundObject backgroundObject, float parallaxFactor) {
        initObject(backgroundObject);
        IShape backgroundObjectSprite = backgroundObject.getShape();
        ParallaxEntity parallaxEntity = new ParallaxEntity(parallaxFactor, backgroundObjectSprite);
        parallaxBackground.attachParallaxEntity(parallaxEntity);
    }

    public void addGameObject(SimpleGameObject gameObject) {
        addUnregisteredObject(gameObject);
        assignGameObjectToGroups(gameObject);
    }

    private void addUnregisteredObject(SimpleGameObject gameObject) {
        initObject(gameObject);
        scene.attachChild(gameObject.getShape());
    }

    private void initObject(SimpleGameObject gameObject) {
        ((MyGameActivity) context).inject(gameObject);
        gameObject.initObject();
    }

    public void removeGameObject(GameObject simpleGameObject) {
        ungroupGameObject(simpleGameObject);
        IShape sprite = simpleGameObject.getShape();
        scene.detachChild(sprite);
    }

    public Player getPlayer() {
        return player;
    }

    public List<GameObject> getAllGameObjects() {
        return allGameObjects;
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    public void ungroupGameObject(GameObject gameObject) {
        allGameObjects.remove(gameObject);
    }

    private void assignGameObjectToGroups(GameObject gameObject) {
        allGameObjects.add(gameObject);
    }

    public SVGSizeService.Rect getSceneBounds() {
        return sceneBounds;
    }

    public void setSceneBounds(SVGSizeService.Rect sceneBounds) {
        this.sceneBounds = sceneBounds;
    }
}
