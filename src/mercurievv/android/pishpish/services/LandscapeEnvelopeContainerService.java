package mercurievv.android.pishpish.services;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.Log;
import com.google.inject.Inject;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.shape.IShape;
import roboguice.inject.ContextSingleton;

import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.26.3
 * Time: 23:40
 */
@ContextSingleton
public class LandscapeEnvelopeContainerService {
    @Inject
    private Camera camera;
    @Inject
    private PathMeasure pathMeasure;
    @Inject
    private PathService pathService;
    @Inject
    private MetricsService metricsService;

    private float[] landscapeY = new float[2560];
    private float[] landscapeAngles = new float[2560];
    private float firstY;
    private float lastY;
    private float firstA;
    private float lastA;

    private double landscapeHeightInPixels;

    //todo think to move metrics coversations outside class
    public void loadLandscape(List<Path> paths, double spriteHeight) {
        float scale = metricsService.getScale();
        this.landscapeHeightInPixels = spriteHeight * scale;
        for (Path landscapePath : paths) {
            pathService.fillAltitudesArray(landscapePath, landscapeY, landscapeAngles, scale);//, scaleX, scaleY
        }
        firstY = landscapeY[0];
        lastY = landscapeY[landscapeY.length - 1];
        fillZeros(landscapeY);
        fillZeros(landscapeAngles);
        landscapeAngles = smoothnesFilter(landscapeAngles);
        firstA = landscapeAngles[0];
        lastA = landscapeAngles[landscapeAngles.length - 1];
        Log.d("Landscape", Arrays.toString(landscapeY));
        Log.d("Landscape", Arrays.toString(landscapeAngles));
    }

    public float getFloorYByX(float x) {
        float svgY = getValue(x, firstY, lastY, landscapeY);
        return convertSvgYToLandscape(svgY);
    }

    public float convertSvgYToLandscape(float svgY) {
        return getLandscapeHeight() + svgY;
    }

    private float getLandscapeHeight() {
        return (float) (camera.getHeight() - landscapeHeightInPixels);
    }

    public float getAngleByX(float x) {
        return getValue(x, firstA, lastA, landscapeAngles);
    }

    float[] smoothnesFilter(float[] floats) {
        int length = floats.length;
        float[] newFloats = new float[length];
        newFloats[0] = (floats[0] + floats[1]) / 2;
        newFloats[length - 1] = (floats[length - 1] + floats[length - 2]) / 2;
        for (int i = 1, landscapeAngleLength = length - 1; i < landscapeAngleLength; i++) {
            float prev = floats[i - 1];
            float next = floats[i + 1];
            float current = (prev + next) / 2;
            newFloats[i] = current;
        }
        return newFloats;
    }


    private void fillZeros(float[] floats) {
        float previous = floats[0];
        for (int i = 0; i < floats.length; i++) {
            if (floats[i] == 0)
                floats[i] = previous;
            previous = floats[i];
        }
    }

    private float getValue(float x, float first, float last, float[] floats) {
        int intX = (int) x;
        if (x < 0)
            return first;
        else if (x >= floats.length)
            return last;
        return floats[intX];
    }

    public float getShapeGroundY(IShape shape) {
        float x = shape.getX();
        float centerX = x + (shape.getWidth() / 2);
        float height = shape.getHeight() * 1.09f;
        return getFloorYByX(centerX) - height;
    }

    public boolean isOverGround(IShape shape) {
        float groundY = getShapeGroundY(shape);
        float shapeY = shape.getY();
        return isOverGround(shapeY, groundY);
    }

    public boolean isUnderGround(float y, float groundY) {
        return y > groundY;
    }

    public boolean isOverGround(float y, float floorY) {
        return y < floorY;
    }
}
