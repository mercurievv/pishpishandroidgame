package mercurievv.android.pishpish.services;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.Log;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.extension.svg.PathSVGHandlerImpl;
import org.anddev.andengine.extension.svg.SVGParser;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.27.4
 * Time: 16:24
 */
@Singleton
public class PathService {
    @Inject
    private PathMeasure pathMeasure;

    public static final int X_INDEX = 0;
    public static final int Y_INDEX = 1;
    private static final int Y_TAN_INDEX = 1;

    public PathSVGHandlerImpl loadPaths(Context context, String filename) {
        AssetManager assets = context.getAssets();
        PathSVGHandlerImpl svgHandler = new PathSVGHandlerImpl();
        try {
            SVGParser.parseSVGFromAsset(assets, filename, svgHandler);
        } catch (IOException e) {
            Log.d("Landscape", "Can't parse landscape svg", e);
        }
        return svgHandler;
    }

    public void fillAltitudesArray(Path landscapePath, float[] landscapeY, float[] landscapeAngles, float scale) {
        pathMeasure.setPath(landscapePath, false);
        int length = (int) pathMeasure.getLength();
        float[] pos = new float[2];
        float[] tan = new float[2];
        for (int i = 0; i < length; i++) { //todo length * scale, i / scale
            pathMeasure.getPosTan(i, pos, tan);
            float x = pos[X_INDEX] * scale;
            float y = pos[Y_INDEX] * scale;
            boolean isHighestPointAlreadyExistForThisX = landscapeY[(int) x] != 0 && landscapeY[(int) x] >= y;
            if (isHighestPointAlreadyExistForThisX)
                continue;
            float tanY = tan[Y_TAN_INDEX];
            addLandscapePoint(x, y, tanY, landscapeY, landscapeAngles);
        }
    }

    public float[] getPathMiddlePoint(Path path, float scale) {
        Line pathLine = getPathLine(path, scale);
        float[] pos1 = new float[2];
        pos1[X_INDEX] = ((pathLine.getX1() + pathLine.getX2()) / 2) * scale;
        pos1[Y_INDEX] = ((pathLine.getY1() + pathLine.getY2()) / 2) * scale;
        Log.d("PathService", Arrays.toString(pos1));
        return pos1;
    }

    public Line getPathLine(Path path, float scale) {
        pathMeasure.setPath(path, false);
        float[] point1 = new float[2];
        float[] tan = new float[2];
        pathMeasure.getPosTan(0, point1, tan);
        float[] point2 = new float[2];
        float length = pathMeasure.getLength();
        pathMeasure.getPosTan(length, point2, tan);
        return new Line(point1[X_INDEX] * scale, point1[Y_INDEX] * scale, point2[X_INDEX] * scale, point2[Y_INDEX] * scale);
    }

    private void addLandscapePoint(float x, float y, float tanY, float[] landscapeY, float[] landscapeAngles) {
        int intX = (int) x;
        landscapeY[intX] = y;
        landscapeAngles[intX] = (float) Math.toDegrees(Math.atan(tanY));
    }
}
