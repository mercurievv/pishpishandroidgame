package mercurievv.andengine;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Matrix;
import android.graphics.Shader;
import org.anddev.andengine.extension.svg.adt.SVGGradient;
import org.xml.sax.Attributes;

import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.1.7
 * Time: 01:40
 */
public class SVGPatternFill extends SVGGradient {
    public static final float DETALIZATION_SCALE = 2f;
    private Bitmap bitmap;

    public SVGPatternFill(String id, Attributes attributes, Bitmap bitmap) {
        super(id, Type.PATTERN, attributes);
        this.bitmap = bitmap;
    }

    @Override
    public Shader createShader() {
        if (this.mShader != null) {
            return this.mShader;
        }

        mShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        this.mMatrix = this.getTransform();
        if (this.mMatrix == null) {
            mMatrix = new Matrix();
        }
        mMatrix.postScale(1/DETALIZATION_SCALE, 1/DETALIZATION_SCALE);
        this.mShader.setLocalMatrix(this.mMatrix);

        return this.mShader;
    }

    @Override
    protected void resolveHref(HashMap<String, SVGGradient> pSVGGradientMap) {
        super.resolveHref(pSVGGradientMap);
        bitmap = ((SVGPatternFill) mParent).bitmap;
    }

    @Override
    protected Matrix getTransform() {
        if (this.mMatrix != null) {
            return this.mMatrix;
        } else {
            final String transfromString = this.mSVGAttributes.getStringAttribute(ATTRIBUTE_PATTERN_TRANSFORM, false);
            return getTransformFromString(transfromString);
        }
    }


}
