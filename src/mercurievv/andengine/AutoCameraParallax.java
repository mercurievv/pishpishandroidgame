package mercurievv.andengine;

import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.shape.IShape;

import javax.microedition.khronos.opengles.GL10;
import java.util.ArrayList;

/**
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga Inc.
 *
 * @author Nicolas Gramlich
 * @since 15:36:26 - 19.07.2010
 */
public class AutoCameraParallax extends ColorBackground {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private final ArrayList<ParallaxEntity> mParallaxEntities = new ArrayList<ParallaxEntity>();
    private int mParallaxEntityCount;

    // ===========================================================
    // Constructors
    // ===========================================================

    public AutoCameraParallax(final float pRed, final float pGreen, final float pBlue) {
        super(pRed, pGreen, pBlue);
    }

    @Override
    public void onDraw(final GL10 pGL, final Camera pCamera) {
        super.onDraw(pGL, pCamera);

        final ArrayList<ParallaxEntity> parallaxEntities = this.mParallaxEntities;

        final float parallaxValue = pCamera.getMinX();
        for (int i = 0; i < this.mParallaxEntityCount; i++) {
            parallaxEntities.get(i).onDraw(pGL, parallaxValue, pCamera);
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public void attachParallaxEntity(final ParallaxEntity pParallaxEntity) {
        this.mParallaxEntities.add(pParallaxEntity);
        this.mParallaxEntityCount++;
    }

    @SuppressWarnings("UnusedDeclaration")
    public boolean detachParallaxEntity(final ParallaxEntity pParallaxEntity) {
        this.mParallaxEntityCount--;
        final boolean success = this.mParallaxEntities.remove(pParallaxEntity);
        if (!success) {
            this.mParallaxEntityCount++;
        }
        return success;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public static class ParallaxEntity {
        // ===========================================================
        // Constants
        // ===========================================================

        // ===========================================================
        // Fields
        // ===========================================================

        final float mParallaxFactor;
        final IShape mShape;

        // ===========================================================
        // Constructors
        // ===========================================================

        public ParallaxEntity(final float pParallaxFactor, final IShape pShape) {
            this.mParallaxFactor = pParallaxFactor;
            this.mShape = pShape;
        }

        // ===========================================================
        // Getter & Setter
        // ===========================================================

        // ===========================================================
        // Methods for/from SuperClass/Interfaces
        // ===========================================================

        // ===========================================================
        // Methods
        // ===========================================================

        public void onDraw(final GL10 pGL, final float pParallaxValue, final Camera pCamera) {
            pGL.glPushMatrix();
            {
                final float shapeWidthScaled = this.mShape.getScaleX();
                float baseOffset = (pParallaxValue * shapeWidthScaled) / this.mParallaxFactor;

                pGL.glTranslatef(baseOffset, 0, 0);
                this.mShape.onDraw(pGL, pCamera);
            }
            pGL.glPopMatrix();
        }

        // ===========================================================
        // Inner and Anonymous Classes
        // ===========================================================
    }
}
