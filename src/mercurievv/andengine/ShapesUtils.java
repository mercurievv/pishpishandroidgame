package mercurievv.andengine;

import com.google.inject.Singleton;
import mercurievv.android.pishpish.entities.statuses.Moving;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.shape.IShape;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.7.7
 * Time: 23:41
 */
@Singleton
public class ShapesUtils {
    public float getDistanceXBetween(IShape shape1, IShape shape2) {
        if(shape1.getX() > shape2.getX()) {
            IShape shapeForSwitching = shape1;
            shape1 = shape2;
            shape2 = shapeForSwitching;
        }
        return (shape2.getX() - shape1.getX()) - shape1.getWidth();
    }
    public float getDistanceBetween(IShape shape1, IShape shape2) {
        float[] xses1 = getXses(shape1);
        float[] xses2 = getXses(shape2);
        float[] yses1 = getYses(shape1);
        float[] yses2 = getYses(shape2);

        if(shape1.getX() > shape2.getX()) {
            IShape shapeForSwitching = shape1;
            shape1 = shape2;
            shape2 = shapeForSwitching;
        }
        return (shape2.getX() - shape1.getX()) - shape1.getWidth();
    }

    private float[] getYses(IShape shape) {
        float[] yes = new float[2];
        yes[0] = shape.getY();
        yes[1] = shape.getY() + shape.getHeight();
        return yes;
    }

    private float[] getXses(IShape shape) {
        float[] xes = new float[2];
        xes[0] = shape.getX();
        xes[1] = shape.getX() + shape.getWidth();
        return xes;
    }

    public Moving.Direction getOppositeDirection(IShape object, IShape from) {
        float objectXCenter = getCenterX(object);
        float fromXCenter = getCenterX(from);
        return objectXCenter - fromXCenter > 0 ? Moving.Direction.RIGHT : Moving.Direction.LEFT;
    }

    private float getCenterX(IShape object) {
        return object.getX() + object.getWidth() / 2;
    }

    private float getCenterY(IShape object) {
        return object.getY() + object.getHeight() / 2;
    }

    public boolean isShapesNear(IShape shape1, IShape shape2, float distance) {
        Rectangle areaAroundShape1 = new Rectangle(
                shape1.getX() - distance
                , shape1.getY() - distance
                , distance * 2 + shape1.getWidth()
                , distance * 2 + shape1.getHeight()
                , null);
        return areaAroundShape1.collidesWith(shape2);
    }
}
