package mercurievv;

import com.xtremelabs.robolectric.RobolectricTestRunner;
import mercurievv.android.pishpish.entities.Boots;
import mercurievv.android.pishpish.entities.Hole;
import mercurievv.android.pishpish.entities.Player;
import mercurievv.android.pishpish.entities.Wall;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.BaseSprite;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.8.7
 * Time: 01:51
 */
@SuppressWarnings("JUnitTestNG")
@RunWith(RobolectricTestRunner.class)
public abstract class Test {
    public IShape createShapeMock(float x, float y, float width, float height) {
        IShape mock = mock(IShape.class);
        mockSizeMethods(mock, x, y, width, height);
        return mock;
    }

    public void mockSizeMethods(IShape mock, float x, float y, float width, float height) {
        when(mock.getX()).thenReturn(x);
        when(mock.getY()).thenReturn(y);
        when(mock.getWidth()).thenReturn(width);
        when(mock.getHeight()).thenReturn(height);
    }

    public GameObject createGameObject(Class<? extends GameObject> classToMock, IShape shape) {
        GameObject recessive = mock(classToMock);
        when(recessive.getShape()).thenReturn(shape);
        return recessive;
    }

    public void mockFlipMethods(BaseSprite baseSprite, Boolean answer) {
        when(baseSprite.isFlippedHorizontal()).thenReturn(answer);
    }

    protected Boots createBoots(final IShape shape) {
        //noinspection deprecation
        return new Boots(0, 0) {
            @Override
            public IShape getShape() {
                return shape;
            }
        };
    }

    protected Player createPlayer(final IShape shape) {
        //noinspection deprecation
        return new Player() {
            @Override
            public IShape getShape() {
                return shape;
            }
        };
    }

    protected Wall createWall(final IShape shape) {
        //noinspection deprecation
        return new Wall(0, 0) {
            @Override
            public IShape getShape() {
                return shape;
            }
        };
    }

    protected Hole createHole(final IShape shape) {
        //noinspection deprecation
        return new Hole(null) {
            @Override
            public IShape getShape() {
                return shape;
            }
        };
    }

    protected Rectangle createRectangle(float x, float y, float width, float height) {
        return new Rectangle(x, y, width, height, null);
    }
}
