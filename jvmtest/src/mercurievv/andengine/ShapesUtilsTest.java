package mercurievv.andengine;

import mercurievv.Test;
import mercurievv.android.pishpish.entities.statuses.Moving;
import org.anddev.andengine.entity.shape.IShape;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.7.7
 * Time: 23:48
 */
@SuppressWarnings("JUnitTestNG")
public class ShapesUtilsTest extends Test {
    private ShapesUtils shapesUtils = new ShapesUtils();
    @org.junit.Test
    public void testGetDistanceXBetween() throws Exception {
        IShape shape1 = createShapeMock(5, 0, 2, 1);
        IShape shape2 = createShapeMock(8, 0, 4, 1);
        float distanceXBetween = shapesUtils.getDistanceXBetween(shape1, shape2);
        assertEquals(1f, distanceXBetween);

        shape1 = createShapeMock(8, 0, 4, 1);
        shape2 = createShapeMock(5, 0, 2, 1);
        distanceXBetween = shapesUtils.getDistanceXBetween(shape1, shape2);
        assertEquals(1f, distanceXBetween);

        shape1 = createShapeMock(5, 0, 3, 1);
        shape2 = createShapeMock(8, 0, 2, 1);
        distanceXBetween = shapesUtils.getDistanceXBetween(shape1, shape2);
        assertEquals(0f, distanceXBetween);

        shape1 = createShapeMock(5, 0, 4, 1);
        shape2 = createShapeMock(8, 0, 2, 1);
        distanceXBetween = shapesUtils.getDistanceXBetween(shape1, shape2);
        assertEquals(-1f, distanceXBetween);
    }

    @org.junit.Test
    public void testOppositeDirectionCalculator() throws Exception {
        IShape object = createShapeMock(2, 0, 1, 1);
        IShape from = createShapeMock(0, 0, 1, 1);
        Moving.Direction direction = shapesUtils.getOppositeDirection(object, from);
        assertEquals(Moving.Direction.RIGHT, direction);

    }

    @org.junit.Test
    public void testIsShapeNear() throws Exception {
        assertTrue(shapesUtils.isShapesNear(createRectangle(0, 0, 1, 1), createRectangle(5, 0, 1, 1), 5));
        assertFalse(shapesUtils.isShapesNear(createRectangle(0, 0, 1, 1), createRectangle(7, 0, 1, 1), 5));

        assertTrue(shapesUtils.isShapesNear(createRectangle(0, 0, 1, 1), createRectangle(-5, 0, 1, 1), 5));
        assertFalse(shapesUtils.isShapesNear(createRectangle(0, 0, 1, 1), createRectangle(-7, 0, 1, 1), 5));

        assertTrue(shapesUtils.isShapesNear(createRectangle(0, 0, 1, 1), createRectangle(0, 5, 1, 1), 5));
        assertFalse(shapesUtils.isShapesNear(createRectangle(0, 0, 1, 1), createRectangle(0, 7, 1, 1), 5));

        assertTrue(shapesUtils.isShapesNear(createRectangle(0, 0, 1, 1), createRectangle(0, -5, 1, 1), 5));
        assertFalse(shapesUtils.isShapesNear(createRectangle(0, 0, 1, 1), createRectangle(0, -7, 1, 1), 5));
    }
}
