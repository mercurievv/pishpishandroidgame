package mercurievv.android.pishpish.services;

import com.xtremelabs.robolectric.RobolectricTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.27.3
 * Time: 02:16
 */
@SuppressWarnings("JUnitTestNG")
@RunWith(RobolectricTestRunner.class)
public class LandscapeEnvelopeContainerServiceTest {
    private LandscapeEnvelopeContainerService landscapeEnvelopeContainerService = new LandscapeEnvelopeContainerService();

    @Test
    public void testGetFloorYByX() throws Exception {
        float y = landscapeEnvelopeContainerService.getFloorYByX(0.1f);
        //3.587
        //49.741
        //0.9470 ;0.7581; 43.44
        //-0.3210 ;-0.3106; -17.8
        //0.944563627243042;
        //0.9462051391601562
        assertEquals(53, y, 3);
    }

    @Test
    public void smooth() {
        String result = Arrays.toString(landscapeEnvelopeContainerService.smoothnesFilter(new float[]{1, 3, 2}));
        assertEquals(Arrays.toString(new float[]{2, 1.5f, 2.5f}), result);
    }
}
