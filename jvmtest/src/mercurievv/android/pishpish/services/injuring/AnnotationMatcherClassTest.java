package mercurievv.android.pishpish.services.injuring;

import com.xtremelabs.robolectric.RobolectricTestRunner;
import mercurievv.android.pishpish.entities.Player;
import mercurievv.android.pishpish.entities.Wall;
import mercurievv.android.pishpish.entities.background.Landscape;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import mercurievv.android.pishpish.entities.statuses.Live;
import mercurievv.android.pishpish.entities.statuses.NotItersectable;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.12.6
 * Time: 14:40
 */
@SuppressWarnings("JUnitTestNG")
@RunWith(RobolectricTestRunner.class)
public class AnnotationMatcherClassTest {
    private AnnotationMatcherClass annotationMatcherClass;
    private boolean isOk;

    @org.junit.Test
    public void testIsApplyable() throws Exception {
        boolean applyable = match(NotItersectable.class, null, new Wall(0, 0), new Player());
        assertTrue(isOk);
        assertTrue(applyable);

        applyable = match(NotItersectable.class, Live.class, new Wall(0, 0), new Landscape());
        assertFalse(isOk);
        assertFalse(applyable);

        applyable = match(NotItersectable.class, Live.class, new Wall(0, 0), new Player());
        assertTrue(isOk);
        assertTrue(applyable);
    }

    private boolean match(Class class1, Class class2, Object object1, Object object2) {
        isOk = false;
        annotationMatcherClass = new AnnotationMatcherClass(class1, class2, new CollisionAction<GameObject, GameObject>() {
            @Override
            public void performAction(GameObject object1, GameObject object2) {
                isOk = true;
            }
        });
        return annotationMatcherClass.isApplyable(object1, object2);
    }
}
