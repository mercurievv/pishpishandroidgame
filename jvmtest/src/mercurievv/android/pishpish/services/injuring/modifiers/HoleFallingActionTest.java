package mercurievv.android.pishpish.services.injuring.modifiers;

import mercurievv.Test;
import org.anddev.andengine.entity.shape.IShape;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.13.6
 * Time: 22:46
 */
@SuppressWarnings("JUnitTestNG")
public class HoleFallingActionTest extends Test{
    private HoleFallingAction holeFallingAction = new HoleFallingAction();

    @org.junit.Test
    public void isObjectInHole() throws Exception {
        IShape injuringShape = createShapeMock(4, 0, 5, 5);

        Set<Integer> fails = fallsInPeriod(injuringShape, 4);
        assertEquals(new HashSet<Integer>(Arrays.asList(new Integer[]{3, 4, 5, 6})), fails);
    }

    @org.junit.Test
    public void testRealCase() throws Exception {
        IShape injuringShape = createShapeMock(299.699f, 221.531f, 79.833954f, 3.1359863f);
        IShape liveShape = createShapeMock(240.55591f, 164.0569f, 61.0f, 58.0f);
        boolean injuring = holeFallingAction.isLiveInsideInjuring(injuringShape, liveShape);
        assertFalse(injuring);

    }

    @org.junit.Test
    public void negativeWidthHeight() throws Exception {
        IShape injuringShape = createShapeMock(5, 5, -5, -5);
        IShape liveShape = createShapeMock(1, 1, 1, 1);
        boolean injuring = holeFallingAction.isLiveInsideInjuring(injuringShape, liveShape);
        assertTrue(injuring);

    }

    private Set<Integer> fallsInPeriod(IShape injuringShape, int liveWidth) {
        IShape liveShape;
        boolean liveInsideInjuring;
        Set<Integer> fails = new HashSet<Integer>();
        for (int i = 0; i < 10; i++) {
            liveShape = createShapeMock(i, 0, liveWidth, 4);
            liveInsideInjuring = holeFallingAction.isLiveInsideInjuring(injuringShape, liveShape);
            if (liveInsideInjuring)
                fails.add(i);
        }
        return fails;
    }
}
