package mercurievv.android.pishpish.services.injuring;

import mercurievv.Test;
import mercurievv.android.pishpish.entities.LiveGameObject;
import mercurievv.android.pishpish.entities.Wall;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import org.anddev.andengine.entity.shape.IShape;

import static org.mockito.Mockito.verify;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.12.6
 * Time: 18:19
 */
@SuppressWarnings("JUnitTestNG")
public class RedoCollisionTest extends Test {
    private RedoCollision redoCollision = new RedoCollision();

    @org.junit.Test
    public void testPerformAction() throws Exception {
        IShape shape = createShapeMock(9, 0, 5, 4);
        GameObject recessive = createGameObject(LiveGameObject.class, shape);
        GameObject dominant = createGameObject(Wall.class, createShapeMock(0, 0, 10, 9));
        redoCollision.performAction(dominant, recessive);
        verify(shape).setPosition(10, 0);

        shape = createShapeMock(-4, 0, 5, 5);
        recessive = createGameObject(LiveGameObject.class, shape);
        dominant = createGameObject(Wall.class, createShapeMock(0, 0, 10, 10));
        redoCollision.performAction(dominant, recessive);
        verify(shape).setPosition(-5, 0);
    }
}
