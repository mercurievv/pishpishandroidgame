package mercurievv.android.pishpish.entities;

import mercurievv.Test;
import mercurievv.android.pishpish.entities.statuses.Moving;
import org.anddev.andengine.entity.modifier.SequenceEntityModifier;
import org.anddev.andengine.entity.sprite.BaseSprite;
import org.junit.Before;

import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertSame;
import static org.mockito.Mockito.mock;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.9.7
 * Time: 00:07
 */
public class BootsTest extends Test {

    @Before
    public void setUp() throws Exception {


    }

    @org.junit.Test
    public void testWalk() throws Exception {
    }

    @org.junit.Test
    public void testFlipIfNeeded() throws Exception {
        Boots boots = createBoots(null);
        SequenceEntityModifier sequenceEntityModifier = mock(SequenceEntityModifier.class);

        BaseSprite baseSprite = createBaseSprite(0, 1, false);
        SequenceEntityModifier result = boots.flipIfNeeded(sequenceEntityModifier, baseSprite, Moving.Direction.RIGHT);
        assertNotSame("No changes was made. Actually object should be flipped and SEM should be created for it.", result, sequenceEntityModifier);

        baseSprite = createBaseSprite(0, 1, true);
        result = boots.flipIfNeeded(sequenceEntityModifier, baseSprite, Moving.Direction.RIGHT);
        assertSame("Changes was made. Actually object shouldn't be flipped and SEM shouldn't be created for it.", result, sequenceEntityModifier);

        baseSprite = createBaseSprite(0, 1, true);
        result = boots.flipIfNeeded(sequenceEntityModifier, baseSprite, Moving.Direction.LEFT);
        assertNotSame("No changes was made. Actually object should be flipped and SEM should be created for it.", result, sequenceEntityModifier);

        baseSprite = createBaseSprite(0, 1, false);
        result = boots.flipIfNeeded(sequenceEntityModifier, baseSprite, Moving.Direction.LEFT);
        assertSame("Changes was made. Actually object shouldn't be flipped and SEM shouldn't be created for it.", result, sequenceEntityModifier);

    }

    private BaseSprite createBaseSprite(final int x, final int width, boolean flipped) {
        final BaseSprite baseSprite = mock(BaseSprite.class);
        mockSizeMethods(baseSprite, x, 0, width, 1);
        mockFlipMethods(baseSprite, flipped);
        return baseSprite;
    }

}
