package mercurievv.android.pishpish.entities.ai;

import mercurievv.Test;
import mercurievv.andengine.ShapesUtils;
import mercurievv.android.pishpish.entities.Boots;
import mercurievv.android.pishpish.entities.statuses.GameObject;
import mercurievv.android.pishpish.entities.statuses.Injuring;
import mercurievv.android.pishpish.services.MetricsService;
import mercurievv.android.pishpish.services.SVGSizeService;
import mercurievv.android.pishpish.services.SceneContainerService;
import org.junit.Before;

import java.util.ArrayList;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.9.7
 * Time: 02:31
 */
@SuppressWarnings("JUnitTestNG")
public class OperationsTest extends Test {
    private Operations operations;

    @Before
    public void setUp() throws Exception {
        MetricsService metricsService = mock(MetricsService.class);
        when(metricsService.getScale()).thenReturn(1f);
        operations = new Operations(new ShapesUtils(), metricsService);

    }

    @org.junit.Test
    public void testNearToSceneBorder() throws Exception {

        SceneContainerService sceneContainerService = new SceneContainerService();
        boolean near;

        sceneContainerService.setSceneBounds(new SVGSizeService.Rect(0, 0, 10, 1));
        near = operations.nearToSceneBorder(sceneContainerService, createShapeMock(5, 0, 3, 1));
        assertFalse(near);

        sceneContainerService.setSceneBounds(new SVGSizeService.Rect(0, 0, 10, 1));
        near = operations.nearToSceneBorder(sceneContainerService, createShapeMock(5, 0, 6, 1));
        assertTrue(near);

        sceneContainerService.setSceneBounds(new SVGSizeService.Rect(0, 0, 10, 1));
        near = operations.nearToSceneBorder(sceneContainerService, createShapeMock(-1, 0, 1, 1));
        assertTrue(near);

    }

    @SuppressWarnings("deprecation")
    @org.junit.Test
    public void testTooCloseToThreatInFront() throws Exception {
        ArrayList<GameObject> possibleThreats = new ArrayList<GameObject>();
        Boots boots;
        Injuring closeThreat;

        boots = createBoots(createRectangle(0, 0, 2, 1));
        possibleThreats.add(createHole(createRectangle(3, 0, 2, 1)));
        closeThreat = operations.getCloseThreat(boots, possibleThreats);
        assertNotNull(closeThreat);

        possibleThreats.clear();
        boots = createBoots(createRectangle(0, 0, 2, 1));
        possibleThreats.add(createHole(createRectangle(300, 0, 2, 1)));
        closeThreat = operations.getCloseThreat(boots, possibleThreats);
        assertNull(closeThreat);
    }
}
